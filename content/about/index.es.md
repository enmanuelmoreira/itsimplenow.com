---
title: "Sobre mí"
date: 2020-07-11T13:18:45+01:00
excludeFromTopNav: false
showDate: false
---

Mi nombre es Enmanuel Moreira. Soy un apasionado por las nuevas tecnologías desde 2006.  

Actualmente me desempeño como DevOps en **[Nisum](https://nisum.com)** en su filial en Santiago de Chile. Hablo español (lengua materna) y portugués (nivel C1)  

He pasado por muchas distribuciones Linux, desde Mandrake, Ubuntu, Debian, Manjaro y de hace un tiempo ~~Fedora~~ (Si, volví a Manjaro :smiley:)  

El contenido del blog va orientado a Administración de Sistemas, Computación en Nube y DevOps, cultura muy en voga en estos días y el cual será, sin duda alguna, el futuro de la Informática.  

Compartir mis experiencias es gratificante, en un lenguaje sencillo, facil de entender y que puedas poner en marcha pequeños proyectos sin mucho esfuerzo.  

## :hammer_and_wrench: Skills

![linux](https://img.shields.io/badge/linux%20-%23FFD133.svg?&style=for-the-badge&logo=linux&logoColor=black) ![aws](https://img.shields.io/badge/AWS%20-%23FF9900.svg?&style=for-the-badge&logo=amazon-aws&logoColor=white) ![ansible](https://img.shields.io/badge/ansible%20-%23EE0000.svg?&style=for-the-badge&logo=ansible&logoColor=white) ![digitalocean](https://img.shields.io/badge/digitalocean%20-%230080FF.svg?&style=for-the-badge&logo=digitalocean&logoColor=white) ![docker](https://img.shields.io/badge/docker%20-%232496ED.svg?&style=for-the-badge&logo=docker&logoColor=white) ![kubernetes](https://img.shields.io/badge/kubernetes%20-%23326ce5.svg?&style=for-the-badge&logo=kubernetes&logoColor=white) ![gcp](https://img.shields.io/badge/gcp%20-%230080FF.svg?&style=for-the-badge&logo=googlecloud&logoColor=white) ![git](https://img.shields.io/badge/git%20-%23F05032.svg?&style=for-the-badge&logo=git&logoColor=white) ![gitlab](https://img.shields.io/badge/gitlab%20ci%20cd%20-%23FCA121.svg?&style=for-the-badge&logo=GitLab&logoColor=white) ![elasticsearch](https://img.shields.io/badge/elasticsearch-%23005571.svg?&style=for-the-badge&logo=elasticsearch&logoColor=white) ![terraform](https://img.shields.io/badge/terraform%20-%23623CE4.svg?&style=for-the-badge&logo=terraform&logoColor=white) ![vagrant](https://img.shields.io/badge/vagrant%20-%231563ff.svg?&style=for-the-badge&logo=vagrant&logoColor=white) ![python](https://img.shields.io/badge/python%20-%233776ab.svg?&style=for-the-badge&logo=python&logoColor=white) ![hugo](https://img.shields.io/badge/hugo-%23FF4088.svg?&style=for-the-badge&logo=hugo&logoColor=white) ![mariadb](https://img.shields.io/badge/mariadb%20-%23203545.svg?&style=for-the-badge&logo=mariadb&logoColor=white) ![jenkins](https://img.shields.io/badge/jenkins%20-%23EE0000.svg?&style=for-the-badge&logo=jenkins&logoColor=black)

![Github stats](https://github-readme-stats.vercel.app/api?username=enmanuelmoreira&show_icons=true&include_all_commits=true&custom_title=Github%20Stats&count_private=true&line_height=20&include_all_commits=true&bg_color=00000000&text_color=777) [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=enmanuelmoreira&layout=compact&card_width=296&bg_color=00000000&text_color=777)](https://github.com/enmanuelmoreira/github-readme-stats)

## :page_with_curl: Certificaciones

<div data-iframe-width="350" data-iframe-height="300" data-share-badge-id="4a4d63fc-d1c1-42ef-9849-7e4831d5d7d7" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="350" data-iframe-height="300" data-share-badge-id="8202621e-e439-4efb-8e61-d13bfca7b58a" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="350" data-iframe-height="300" data-share-badge-id="0eba590f-21c3-42ee-b439-0be196e55912" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

## :page_with_curl: Certificaciones Anteriores

<div data-iframe-width="480" data-iframe-height="300" data-share-badge-id="da01b2ef-c8ac-4ba9-a195-9fb92b803301" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="415" data-iframe-height="300" data-share-badge-id="211689ab-e201-49bc-a934-3856ec542455" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

<div data-iframe-width="480" data-iframe-height="300" data-share-badge-id="a00eb24a-5889-4724-b287-99f5c750b9c9" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

## :call_me_hand: Contáctame

[![itsimplenow.com@gmail.com](https://img.shields.io/badge/itsimplenow@gmail.com%20-%23EA4335.svg?&style=for-the-badge&logo=gmail&logoColor=white)](mailto:itsimplenow@gmail.com)
[![linkedin](https://img.shields.io/badge/LinkedIn%20-%232867b2.svg?&style=for-the-badge&logo=LinkedIn&logoColor=white)](https://www.linkedin.com/in/enmanuelmoreira)
[![telegram](https://img.shields.io/badge/itsimplenow%20-%232CA5E0.svg?&style=for-the-badge&logo=Telegram&logoColor=white)](https://t.me/tsimplenow)

## :link: Otros links

[![github](https://img.shields.io/badge/GitHub%20-%23203545.svg?&style=for-the-badge&logo=github&logoColor=white)](https://www.github.com/enmanuelmoreira)
[![docker hub](https://img.shields.io/badge/Docker%20Hub%20-%232496ED.svg?&style=for-the-badge&logo=docker&logoColor=white)](https://hub.docker.com/u/enmanuelmoreira)
[![twitter](https://img.shields.io/badge/enmanuelmoreira%20-%232496ED.svg?&style=for-the-badge&logo=twitter&logoColor=white)](https://www.twitter.com/enmanuelmoreira)
[![mastodon](https://img.shields.io/badge/@enmanuelmoreira%20-%232B90D9.svg?&style=for-the-badge&logo=mastodon&logoColor=white)](https://mastodon.online/@enmanuelmoreira)
[![twitch](https://img.shields.io/badge/enmanuelmoreira%20-%239147FE.svg?&style=for-the-badge&logo=twitch&logoColor=white)](https://www.twitch.tv/enmanuelmoreira)
[![youtube](https://img.shields.io/badge/itsimplenow%20-%23FF0000.svg?&style=for-the-badge&logo=youtube&logoColor=white)](https://www.youtube.com/)


## :books: Cursos y Libros

[![leanpub](https://img.shields.io/badge/leanpub%20-%23203545.svg?&style=for-the-badge&logo=leanpub&logoColor=white)](https://www.leanpub.com/)
[![udemy](https://img.shields.io/badge/udemy%20-%23ED5252.svg?&style=for-the-badge&logo=udemy&logoColor=white)](https://www.udemy.com/)

## :money_with_wings: Dona

Si encuentras este sitio web útil, puedes invitarme un Café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>
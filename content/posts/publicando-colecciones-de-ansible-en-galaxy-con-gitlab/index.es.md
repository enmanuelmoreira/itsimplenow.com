---
title: "Publicando Colecciones de Ansible en Galaxy con GitLab"
date: 2022-01-31
lastmod: 2022-01-31
authors: [enmanuelmoreira]
description: "Una de las maneras más fáciles para poder compartir Roles y Collections es la plataforma Ansible Galaxy, sin embargo, por alguna razón solo nos permite autenticarnos a través de GitHub. En este articulo te voy a mostrar como publicar tus collections en GitLab de manera automática con GitLab CI."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.png"

categories: ["Ansible","DevOps","GitLab"]
tags: ["ansible","gitlab","devops","automatizacion","galaxy"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

## Introducción

***

[Ansible](https://www.ansible.com/) es una de mis herramientas favoritas para configurar servidores en masa, y a diferencia de Chef o Puppet, Ansible no necesita un agente para poder ejecutar las tareas, solo se necesita tener ssh instalado en los servidores remotos para poder hacer su trabajo.

Una de las maneras más fáciles para poder compartir y descargar Roles y Collections es la plataforma [Ansible Galaxy](https://galaxy.ansible.com/), sin embargo, por alguna razón solo nos permite autenticarnos a través de GitHub. En este articulo te voy a mostrar como publicar tus collections en GitLab de manera automática con GitLab CI.

## Preparando el Terreno

En este artículo estoy asumiendo que sabes Ansible y entiendes el concepto de [Integración Continua](https://es.wikipedia.org/wiki/Integraci%C3%B3n_continua). De todas maneras voy a ser lo más específico posible para que el usuario mas novel pueda hacerlo.

Asumo también que tienes cuentas creadas tanto en GitHub como en GitLab para poder avanzar.

## Instalando Ansible en Nuestro Equipo

Dependiendo de tu distribución, la instalación será un poco diferente. En este caso vamos a instalar Ansible directamente desde **pip**:

```bash
sudo pip3 install ansible
```

## Creando la Cuenta en Ansible Galaxy

Podemos crear nuestra cuenta en Ansible Galaxy yendo al sitio: **<https://galaxy.ansible.com>**, vamos a la esquina superior derecha en la opción **Login**:

![Login](/images/ansible-galaxy-gitlab/galaxy-0.png "Ansible Galaxy")

Iniciamos sesión con nuestra cuenta GitHub:

![GitHub](/images/ansible-galaxy-gitlab/galaxy-1.png "Inicio de Sesión")

![GitHub](/images/ansible-galaxy-gitlab/galaxy-2.png "Ingresamos credenciales")

Una vez iniciamos sesión, nos redirigirá de nuevo a la página de Ansible Galaxy, vamos de nuevo a la esquina superior derecha y hacemos click en nuestro nombre de usuario, seguido en Preferences:

![Galaxy](/images/ansible-galaxy-gitlab/galaxy-3.png "Preferencias")

Y vamos a ver nuestra API Key, vamos a copiarla y reservarla para configurarla en GitLab.

![Preferencias](/images/ansible-galaxy-gitlab/galaxy-4.png "Copiamos API Key")

{{< admonition warning >}}
Recuerda que la API Key es una clave privada por lo que debes tratarla como si fuera tu clave personal.
{{< /admonition >}}

## Creando la Colección

- Creamos una colección de Ansible en nuestro equipo:

```bash
ansible-galaxy collection init usuario.micoleccion
```

Lo cual creará una colección con la siguiente estructura:

```bash
.
├── docs
├── galaxy.yml
├── plugins
│   └── README.md
├── README.md
└── roles
```

**docs**: en este directorio almacenamos la documentación de la colección.  
**plugins**: en este directorio se almacenan plugins de Ansible.  
**roles:** en este directorio almacenamos los roles que se ejecutarán en la colección.  
**galaxy.yml**: es el manifiesto de la colección. El cual tiene la siguiente estructura:

<!---
{{< gitlab-snippet src="https://gitlab.com/-/snippets/2244443.js" >}}
-->

{{< pastebin id="dJBVGLY1" height="500px" >}}

Ajustamos el archivo galaxy.yml a nuestras necesidades:

<!---
{{< gitlab-snippet src="https://gitlab.com/-/snippets/2244446.js" >}}
-->

{{< pastebin id="x0A99Z0n" height="500px" >}}

Creamos también una carpeta en la raiz de la colección llamada `meta` con el archivo `runtime.yml`:

```bash
mkdir meta
touch meta/runtime.yml
```

Y pegamos el siguiente contenido:

```yaml
---
requires_ansible: '>=2.9.10'
```

## Creando los Roles

Entramos en la carpeta **roles** y vamos a crear un par de roles:

```bash
cd roles
```

```bash
ansible-galaxy role init software
```

```bash
ansible-galaxy role init download
```

Tendremos entonces siguiente estructura:

```bash
.
├── download
│   ├── defaults
│   │   └── main.yml
│   ├── files
│   ├── handlers
│   │   └── main.yml
│   ├── meta
│   │   └── main.yml
│   ├── README.md
│   ├── tasks
│   │   └── main.yml
│   ├── templates
│   ├── tests
│   │   ├── inventory
│   │   └── test.yml
│   └── vars
│       └── main.yml
└── software
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
```

Para ahorranos tiempo, vamos a editar el archivo `tasks/main.yml` en cada rol, con una tarea sencilla:

En el Rol **download** vamos realizar una tarea que descargue una imagen de [Debian](https://www.debian.org/) en el directorio /tmp:

```yaml
- name: Descargando Imagen minima de debian
  get_url:
    url: https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.2.0-amd64-netinst.iso
    dest: /tmp
    mode: 0755
```

En el rol **software** vamos a instalar MariaDB y Apache:

```yaml
---
- name: Instalando MariaDB y apache.
  pkg:
    name:
      - httpd
      - mariadb-server
    state: latest
```

## Subiendo Nuestra Colección a GitLab

En esta etapa vamos a crear el proyecto en GitLab y luego subir los cambios realizados.  

### Creando Proyecto

Vamos a la página de GitLab (donde ya devemos haber iniciando sesión) Esquina superior derecha, **Nuevo Proyecto**:

![Nuevo Proyecto](/images/ansible-galaxy-gitlab/gitlab-0.png "Nuevo Proyecto")

Creamos un proyecto en blanco:

![Proyecto en blanco](/images/ansible-galaxy-gitlab/gitlab-1.png "Crear un proyecto en blanco")

Le damos un nombre, una descripción y destildamos las opciones Crear README y SAST.

![Crear proyecto](/images/ansible-galaxy-gitlab/gitlab-2.png "Crear Proyecto")

Una vez creado, nos redireccionará a la página principal del proyecto, Vamos a la parte Izquierda en **Configuración -> CI / CD**

![Configuracion](/images/ansible-galaxy-gitlab/gitlab-3.png "Configuración -> CI / CD")

Bajamos un poco y nos encontraremos con la opción variables. Damos click a Expandir y click en la opción **Añadir Variable**, aquí vamos a añadir la API Key que sacamos de Ansible Galaxy para que GitLab construya la colección y se autentique con Galaxy para publicarla.

![Configuracion](/images/ansible-galaxy-gitlab/gitlab-4.png "Variables")

En **Clave** colocamos el nombre con que queremos guardar la variable, en este caso **GALAXY_TOKEN**.  
En **Valor** colocamos la API Key de Ansible Galaxy.  

Tildamos ambas opciones de: **Proteger Variable** y **Enmascarar Variable**. La primera nos va a permitir que esa variable solo pueda ser accedida por este repositorio y no por otros. y al segunda opción va a evitar que la API Key se muestre por el output del Pipeline, así nos cercioramos de no exponer la API Key a otros ojos.

![Configuracion](/images/ansible-galaxy-gitlab/gitlab-5.png "Añadir Variable")

Listo, quedaría asi:

![Configuracion](/images/ansible-galaxy-gitlab/gitlab-6.png "")

### Creando el Pipeline de Gitlab

Ahora viene la parte interesante. Vamos a crear el pipeline con las instrucciones que debe seguir GitLab CI para que nos construya el paquete de la colección y lo publique en Ansible Galaxy todo de manera automática.

Creamos el archivo `.gitlab-ci.yml` en la raíz de la colección. Veremos uno por uno las instrucciones al detalle:

<!---
{{< gitlab-snippet src="https://gitlab.com/-/snippets/2244449.js" >}}
-->

{{< pastebin id="rKwEVf8X" height="500px" >}}

```yaml
variables:
  GALAXY_NAME: micoleccion
```

La clave `variables` nos permite predefinir variables que se van a ejecutar a lo largo del pipeline. De verdad que nos ahorra un montón de trabajo si es que una variable es utilizada por varios stages.  

En este caso particular, para efectos prácticos, estamos definiendo una variable `GALAXY_NAME` con el nombre de nuestra colección que nos va a servir en la etapa de build para nombrar el paquete que contiene la colección. Podríamos usar una variable definida de GitLab llamada `CI_PROJECT_NAME` y a menos que el repositorio se llame tal cual como Galaxy pide que sean los nombres de las colecciones, la puedes utilizar. En caso contrario, nos toca definir esta variable que cumple con lo que Galaxy pide en términos de nombrar los archivos.  

```yaml
ìmage: enmanuelmoreira/docker-ansible-alpine:latest
```

Es una imagen Docker que contiene Ansible instalado. Estoy utilizando una que ya construí y que puede ver el código fuente en <https://gitlab.com/enmanuelmoreira/docker-ansible-alpine>  

```yaml
stages:
  - build
  - publish
```

Los **stages** son las etapas en la que el pipeline ejecutará los pasos contenidos. En este caso tenemos dos etapas: build y publish.

En la etapa **build** vamos a empaquetar la colección utilizando una serie de instrucciones:

```yaml
build:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - ansible-galaxy collection build --force
  artifacts:
    paths:
      - $CI_PROJECT_NAMESPACE-$GALAXY_NAME-$CI_COMMIT_TAG.tar.gz
    expire_in: 30 days
```

**rules**: se ejecutará la etapa build solo si cumple con las reglas establecidas. En este caso vamos a restringirla al tag del commit (en un momento explico el porqué)  
**script**: aquí vamos a ejecutar los comandos para construir el paquete que se enviará a Galaxy.  
**artifacts**: es el archivo resultante de la construcción. Para que el paquete cumpla con los requerimientos de nombres de Galaxy, vamos a utilizar una variable que nos provee GitLab: `$CI_PROJECT_NAMESPACE` que nos proveerá el nombre de usuario del repo (el dueño del repo), seguidor de un guión y luego la variable que definimos previamente en el encabezado del manifiesto `$GALAXY_NAME`, seguido de otro guión con la variable del commit tag `$CI_COMMIT_TAG` y al extensión del archivo `.tag.gz`

Este "artefacto" o archivo resultante se va a almacenar en GitLab por 30 días con la clave `expire_in`

En la etapa **publish** vamos a autenticarnos con Ansible Galaxy y publicar nuestra colección empaquetada:

```yaml
publish:
  stage: publish
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - build
  script:
    - ansible-galaxy collection publish ./$CI_PROJECT_NAMESPACE-$GALAXY_NAME-$CI_COMMIT_TAG.tar.gz --token $GALAXY_TOKEN
```

Bueno, aquí tenemos algo nuevo `dependencies`. Esta clave le dice a GitLab que esta etapa solo se va a ejecutar si la etapa build tuvo éxito.

En `script` podemos ver el comando con el cual vamos a publicar nuestra colección, con el nombre del artefacto tal cual como lo hicimos en la etapa de build y el argumento final sería proporcionar el token de Galaxy y que previamente lo guardamos como una variable dentro de GitLab.

### Subiendo los Cambios

Perfecto. Solo nos queda subir los cambios que hemos realizado a GitLab:

Inicializamos el repo con la rama main por defecto:

```bash
git init --initial-branch=main
```

Añadimos el repositorio de GitLab con nuestras credenciales (sustituir usuario por tu usuario)

```bash
git remote add origin git@gitlab.com:usuario/ansible-collection-micoleccion.git
```

Añadimos los archivos de la colección

```bash
git add .
```

Hacemos commit de los cambios:

```bash
git commit -m "Initial commit"
```

Etiquetamos la version 1.0.0 de nuestra colección:

```bash
git tag -a 1.0.0 -m "Version 1.0.0"
```

Subimos los cambios:

```bash
git push -u origin main
```

Por ultimo subimos los cambios del tag 1.0.0:

```bash
git push -u origin 1.0.0
```

¿Recuerdas que te mencioné que restringimos la ejecución del pipeline al tag o etiquetas? La razón es que Galaxy necesita saber que versión de la colección se está enviando, por lo que, además de colocarla en el manifiesto `galaxy.yml` esta debe coincidir con el nombre del paquete y etiquetando o colocándole tag a la versión del paquete podemos tener ese dato e incluirlo. Por consiguiente, cada vez que realicemos un cambio a nuestra colección y esta pase a ser la versión 1.1.0 por ejemplo, debemos hacer `git tag 1.1.0` y ejecutar `git push -u origin 1.1.0` y asi GitLab nos creará el tag de version en nuestro repo y ejecutará el pipeline.

### Monitoreando el Pipeline

Vamos a GitLab en la parte izquierda en el Menu CI/CD -> Pipelines:

![Pipelines](/images/ansible-galaxy-gitlab/gitlab-7.png "")

Una vez dentro, veremos todos los pipelines que se han ejecutado hasta el momento:

![Pipelines](/images/ansible-galaxy-gitlab/gitlab-8.png "Pipelines Ejecutados")

Vamos a entrar en el primero y aprovechar de hacer un poco de troubleshooting:

![Pipelines](/images/ansible-galaxy-gitlab/gitlab-9.png "Pipelines Ejecutados")

Entremos al stage build que si se ejecutó correctamente:

![build](/images/ansible-galaxy-gitlab/gitlab-10.png "Stage build")

Si observamos bien, está todo correcto, y si te fijas en la parte derecha, hay tres botones: Mantener, Descargar y Explorar, esto nos permite ver nuestro artefacto recien creado (que no es mas que la coleccion empaquetada), descargarla, etc. Durante 30 dias va a permanecer almacenado en GitLab porque asi se lo hemos dicho en el archivo `.gitlab-ci.yml`

Vamos ahora a ver que pasó con el stage publish:

![publish](/images/ansible-galaxy-gitlab/gitlab-11.png "Stage publish")

Pues por alguna razón el comando falló al no encontrar el token, pero si ya lo habiamos configurado en GitLab con su respectiva variable llamada `$GALAXY_TOKEN`, ¿Qué ha podido suceder entonces?

¿Recuerdas cuando al momento de configurar el token como variable activamos la opción **Proteger Variable**? Nos limita a utilizar las ramas del repositorio que estén protegidas. Por defecto los tags no vienen protegidos y por ende, si configuramos un tag la 1.0.1 por ejemplo, al actualizar a la 1.0.2 no se va a ejecutar el pipeline porque el tag 1.0.2 no estaría protegido. Una solución a esto seria agregar de una vez todos los tags que se suban a continuación.

Para esto tenemos que ir a **Configuración -> Repositorio**, en la opción **Protected Tags**, donde dice etiqueta le colocamos un asterisco * escogemos **Create wildcard**, Autorizado a crear: Mantainers (puedes colocarte a ti mismo o un miembro del equipo) y click en **Proteger**:

![publish](/images/ansible-galaxy-gitlab/gitlab-12.png "Crear wilddard")

![publish](/images/ansible-galaxy-gitlab/gitlab-13.png "Proteger tags")

Ahora nos vamos de nuevo a CI/CD -> Pipelines, vamos a el stage que falló (publish) y hacemos click en Reintentar:

![publish](/images/ansible-galaxy-gitlab/gitlab-14.png "Reintentamos el stage")

Y voilá, se ha ejecutado el stage publish:

![publish](/images/ansible-galaxy-gitlab/gitlab-15.png "Stage Terminado")

### Revisando en Ansible Galaxy

Para terminar, nos aseguramos que nuestra colección se encuentra publicada, nos vamos a la página de Ansible Galaxy, en el menú de la izquierda a **My Contents**:

![My Content](/images/ansible-galaxy-gitlab/galaxy-5.png "")

Ahí lo tenemos:

![My Content](/images/ansible-galaxy-gitlab/galaxy-6.png "")

Si queremos ver más detalles, hagamos click en el nombre de la colección **micoleccion**

![My Content](/images/ansible-galaxy-gitlab/galaxy-7.png "")

Y con eso estaríamos.

## Conclusión

GitLab nos permite poder publicar nuestras colecciones a Ansible Galaxy de manera fácil y automatizada a través de GitLab CI. Si quisieramos extender más nuestro pipeline y colocarle pruebas con molecule, por ejemplo, lo podríamos hacer, pero eso sería tema para otro artículo :smile:

El código del pipeline como de la coleccion de prueba, quedará en el repositorio:  
**<https://gitlab.com/enmanuelmoreira/ansible-collection-micoleccion>**

Espero les haya gustado y nos vemos en la próxima!

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

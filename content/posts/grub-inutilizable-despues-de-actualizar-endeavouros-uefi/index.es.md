---
title: "Grub Inutilizable Después de Actualizar EndeavourOS (UEFI)"
date: 2022-08-28
lastmod: 2022-08-28
authors: [enmanuelmoreira]
description: "Hoy me acaba de ocurrir un pequeño incidente actualizando EndeavourOS. La versión de Grub 2.06.r322 hace que este se vuelva inutilizable, por lo que les cuento como hice para solucionarlo."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpeg"

categories: ["Linux"]
tags: ["uefi","grub","linux"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

***

Hoy me acaba de ocurrir un pequeño incidente actualizando EndeavourOS. La versión de Grub 2.06.r322 hace que este se vuelva inutilizable, por lo que les cuento como hice para solucionarlo.  

{{< admonition warning >}}
Es posible pérdida de datos al ejecutar los comandos a continuación. Por lo que sugiero hacer una copia de seguridad antes de proceder.
{{< /admonition >}}

## Crear Live USB de EndeavourOS

Primero descargamos la imagen ISO de EndeavourOS desde la página oficial: <https://github.com/endeavouros-team/ISO/releases/download/1-EndeavourOS-ISO-releases-archive/EndeavourOS_Artemis_neo_22_7.iso>

Ahora creemos el Live USB, hay varias alternativas pero la que más me gusta es con [dd](https://man7.org/linux/man-pages/man1/dd.1.html) Necesitaremos un pendrive de al menos 8GB de espacio, conectamos el pendrive al equipo y averiguemos en que dispositivo lo reconoce Linux con ```fdisk```:

```bash
sudo fdisk -l
```

En mi caso particular, es un USB de 32GB el cual lo reconoce como ```/dev/sdc```:

```bash
Disco /dev/sdc: 28,82 GiB, 30943995904 bytes, 60437492 sectores
Modelo de disco: DataTraveler 3.0
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: dos
Identificador del disco: 0x593e4509

Disposit.  Inicio Comienzo   Final Sectores Tamaño Id Tipo
/dev/sdc1  *            64 3564735  3564672   1,7G  0 Vacía
/dev/sdc2          3564736 3777727   212992   104M ef EFI (FAT-12/16/32)
```

Ya con esta información, procederemos a crear el Live USB:

```bash
sudo dd if=/home/usuario/EndeavourOS_Artemis_neo_22_7.iso of=/dev/sdc bs=4k status=progress
```

Donde:

* `if=/home/usuario/EndeavourOS_Artemis_neo_22_7.iso` es la ruta donde se encuentra ubicado nuestra imagen ISO de EndeavourOS.
* `of=/dev/sdc` es el dispositivo que Linux reconoce como nuestro pendrive.
* `bs=4k` es un parametro en el que le indicamos que lea y escriba de a 4 Kilobytes entre origen y destino.
* `status=progress` que nos muestre el progreso de la escritura.

## Arrancar el Live USB y montando Sistema de Archivos

Arranquemos el Live USB y cuando haya cargado, abrimos una terminal y ejecutamos `fdisk` para saber en que disco se encuentra la partición de arranque:

```bash
sudo fdisk -l
```

```bash
Disco /dev/sdb: 447,14 GiB, 480113590272 bytes, 937721856 sectores
Modelo de disco: WDC WDS480G2G0B-
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: gpt
Identificador del disco: 7855884A-F57E-4513-8930-CB23D4ABF296

Disposit.  Comienzo     Final  Sectores Tamaño Tipo
/dev/sdb1      2048   2099199   2097152     1G Sistema EFI
/dev/sdb2   2099200 937721822 935622623 446,1G Sistema de ficheros de Linux
```

Nuevamente en mi caso particular, tengo mi partición de arranque separada de la partición Raíz. En caso de que ninguna partición que tengas te muestra el Tipo: Sistema  EFI, es porque lo tienes en una sola partición.

Ahora, montamos tanto la partición Raíz como la partición EFI en el directorio `/mnt`:

```bash
sudo mount /dev/sdb2 /mnt/boot/efi
sudo mount /dev/sdb1 /mnt
```

Solo en caso que tuvieres una sola partición:

```bash
sudo mount /dev/sdX1 /mnt
```

Y luego entramos en modo chroot al sistema de archivos:

```bash
sudo arch-chroot /mnt
```

## Reinstalando Grub

A partir de aquí es solo ejecutar:

```bash
sudo grub-install
```

Tardará unos segundos hasta que haya instalado Grub nuevamente.

Una vez finalice, nos desconectamos del sistema de archivos:

```bash
exit
```

Y reiniciamos nuestro equipo.

Si por alguna razón, aún no me hemos actualizado nuestro sistema, una vez realizado, ejecutamos:

```bash
sudo grub-install
```

Para que vuelva a regenerar el grub y no sea necesario tener que hacer los pasos anteriores.

## ¿Qué Causó Todo esto?

Según podemos leer en esta [entrada](https://forum.endeavouros.com/t/full-transparency-on-the-grub-issue/30784) del blog de EndeavourOS, la falla la ocasionó con un commit desde el proyecto Grub donde se introduce una nueva llamada a `fwsetup --is-supported` en el archivo `/etc/grub.d/30_uefi-firmware` la cual en la versión de grub que tenemos instalada antes de la actualización no soportaba, lo cual ocasionará que falle al cargar el menu de arranque, por lo que se tiene que hacer de manera manual la reinstalación de grub.

Espero les haya gustado, ¡hasta la próxima!

## Referencias

* Arch-chroot for EFI/UEFI systems: <https://discovery.endeavouros.com/system-rescue/arch-chroot-for-efi-uefi-systems/2021/03/>
* The latest grub package update needs some manual intervention: <https://forum.endeavouros.com/t/the-latest-grub-package-update-needs-some-manual-intervention/30689>

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

---
title: "Guía de Estudio para la Certificacion Google Associate Cloud Engineer"
date: 2021-12-30
lastmod: 2021-12-30
authors: [enmanuelmoreira]
description: "En esta guia podras encontrar un resumen del material necesario para estudiar y presentar la certificacion Google Associate Cloud Engineer."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpg"

categories: ["Certificaciones"]
tags: ["gcp","cloud","certificaciones"]

lightgallery: true
---

<!--more-->

# Atencion: Work In Progress

# TODO:

- Cloud functions
- GKE
- IAM
- Stackdriver
- Facturacion
- Cloud Deployment Manager
- Cloud Logging

Estare actualizando el contenido porque aun me faltan

## 📕 Glosario de Términos

- Aplicación Multicapa (Multi-Tier Application)  
  - En ingenieria de software, una arquitectura multiier (comunmente referida como arquitectura n-tier) o arquitectura multicapa es una arquitectura cliente-servidor en la que las funciones de presentación, procesos y gestión de datos de la aplicación están fisicamente separados. - [wiki](https://es.wikipedia.org/wiki/Programaci%C3%B3n_por_capas)

- Zone vs Regional
  - Las Regiones están formadas por Zonas (por ejemplo Región us-west1 está firmada por las Zonas us-west1-[a,b,c])- [docs](https://cloud.google.com/compute/docs/regions-zones)
  - Todas las regiones están a una distancia mínima de 160 kilómetros. - [doc](https://cloud.google.com/storage/docs/locations#location-r)

- db transactional**:** realiza todas las transacciones o ninguna (cuenta con rollback)

- OLAP vs OLTP - [artículo](https://bitalksbi.com/oltp-vs-olap-database-vs-data-warehouse)
  - OLTP: OnLine Transaction Process (Base de Datos)
    - El propósito de un sistema de OLTP system es la de manejar datos
  - OLAP: Online Analytical Processing (Almacen de datos - Data Warehouse)
    - Usado para el análisis de datos

- Gcloud
  - Gestiona los recursos de Google Cloud Platform además del flujo de desarrollo - [doc](https://cloud.google.com/sdk/gcloud/reference)

- Network Address Translation (NAT)
  - Mapea una Dirección IP dentro de otra distinta - [wiki](https://es.wikipedia.org/wiki/Traducci%C3%B3n_de_direcciones_de_red)
  - Se usa para mapear direcciones IP privadas (192.168.x.x) a una IP externa pública.
  - Como NAT puede gestionar multiples conecciones desde los dispositivos dentro de la red, usando la misma dirección IP externa? (es decir: cómo NAT puede redireccionar los paquetes recibidos?)
    - Cada paquete tiene una IP:puerto del emisor, NAT reemplaza esto con un puerto personalizado y lo vincula con la IP del dispositivo interno - [reddit](https://www.reddit.com/r/explainlikeimfive/comments/1wqc30/eli5_how_does_nat_network_address_translation_work/)

- Proxy
  - el cliente direcciona la petición al servidor proxy, cual cual evalua la petición y realiza las transacciones de red requeridas - [wiki](https://es.wikipedia.org/wiki/Servidor_proxy)
    - “En Redes de computadoras, un proxy es un intermediario en el que sé es asignado para enviar y recibir los mensajes por nosotros.” - [reddit](https://www.reddit.com/r/explainlikeimfive/comments/yhyzv/eli5_what_is_a_proxy_how_do_i_get_one_and_why_do/)
  - Proxy vs nat
    - “NAT opera en la capa de red mientras que el proxy trabaja en la capa de aplicación.” - [huawei](https://support.huawei.com/enterprise/en/knowledge/KB1000081311/)
      - NAT es transparente para muchas aplicaciones
        - El proxy debe recurrir a la dirección IP del servidor proxy especificado en los programas de aplicación.

- Recursos Globales - [doc](https://cloud.google.com/compute/docs/regions-zones/global-regional-zonal-resources#globalresources)
  - Los recursos globales en GCP son accesibles para cualquier recurso en cualquier zona dentro del mismo proyecto.
  - ⭐ Algunos Recursos Globales:
    - Imágenes: usadas por cualquier instancia o recurso de disco en el mismo proyecto como imagen
    - Instantáneas
    - Redes de VPC
    - Rutas
- Servicio de Google Front End - [doc](https://cloud.google.com/security/infrastructure/design#google_front_end_service)
  - Cuando se quiere publicar un servicio en Internet para que esté disponible, puede registrarse con un servicio de infraestructura llamado Google Frontend.

## 🔢 Gcloud Básico

El SDK de Cloud es un conjunto de herramientas que puedes usar para administrar recursos y aplicaciones alojados en Google Cloud. Estas herramientas incluyen las herramientas de línea de comandos de gcloud, gsutil y bq. [docs](https://cloud.google.com/sdk/docs/)

{{< gist enmanuelmoreira 3dca5f31625c8c19cc0e8bc7a1fb25bb >}}

## 🌐 Redes

### Nube Privada Virtual (VPC)

- Una red Nube Privada Virtual (VPC) es una versión virtual de una red física, implementada dentro de la red de producción de Google, utilizando Andromeda.

- O: es un conjunto de recursos computacionales configurables por demanda al interior de un ambiente de computación en la nube pública, el cual provee un cierto nivel de aislamiento entre las diferentes organizaciones o usuarios que utilizan dichos recursos - [wiki](https://es.wikipedia.org/wiki/Nube_privada_virtual)

- La red de VPC consta de una o más particiones de rango de direcciones IP útiles llamadas subredes

- ⚠️ Redes y subredes son recursos diferentes en Google Cloud - [doc](https://cloud.google.com/vpc/docs/using-vpc#gcloud_3)
  - Las redes VPC no tienen rangos de direcciones IP asociadas a el

- Las VPC son recursos globales y las subredes dentro de esa VPC son recursos regionales - [wiki](https://en.wikipedia.org/wiki/Virtual_private_cloud)
  - VPC en modo-auto crea una subred para cada Región
  - Rango CIDR: cuanto menor sea el número después de la barra, más direcciones estarán disponibles

- VPC Compartidos
  - Permite a una organización conectar recursos de varios proyectos a una VPC común - [gcp](https://cloud.google.com/vpc/docs/shared-vpc)
  - Cada recurso puede comunicarse con otro usando la dirección IP interna desde esa red
  - ⭐ Uso: designa un proyecto como proyecto host y se adjunta otro servicio a el

- VPC Network peering
  - Se utiliza para conectar dos VPC independientemente de que pertenezcan al mismo proyecto o a la misma organización. - [doc](https://cloud.google.com/vpc/docs/vpc-peering)
  - Caracteristicas - [doc](https://cloud.google.com/vpc/docs/vpc-peering#key_properties)
    - VPC Network Peering trabaja con Compute Engine, GKE, App Engine Flexible.
    - ⭐ Nota: App Engine Standard no es compatible (solo Flexible)

- Rango de Alias IP
  - Utilizado para asignar múltiples IP to a MV
    - útil si el recurso aloja varios servicios y desea asignar a cada servicio una IP diferente (útil para los pods de GKE)

- Subredes
  - Cada red de VPC consta de una o más particiones útiles del rango de IP llamadas subredes - [doc](https://cloud.google.com/vpc/docs/vpc#vpc_networks_and_subnets)
  - Cada subred está asociada a una región
  - Para saber cuales son las subredes por defecto:
    - ```gcloud compute networks subnets list --network default```  
  
- ⭐ Regiones y Zonas disponobles
  - Hasta el primer trimestre de 2021, GCP está disponible en 28 regiones con 85 zonas - [doc](https://cloud.google.com/about/locations)

- Rutas - [doc](https://cloud.google.com/vpc/docs/routes#routingpacketsinternet)
  - ruta predeterminada generada por el sistema:
    - Prioridad de 1000 y target 0/0
    - ruta de salida de la red de VPC, incluida la ruta a Internet
    - ruta estándar para Acceso Privado a Google

- ⭐ Acceso Privado a Google - [doc](https://cloud.google.com/vpc/docs/configure-private-google-access)
  - Permite a una MV sin dirección IP externa a comunicarse con las APIs y servicios de Google

- Una red debe tener al menos una subred antes de poder utilizarla.
  - Puede crearse con el siguiente comando:

```bash
# Crea la Red VPC:
gcloud compute networks create NOMBRE_RED \
    --subnet-mode=auto \ # auto o custom
    --bgp-routing-mode=DYNAMIC_ROUTING_MODE \ # global o regional
    --mtu=MTU # 
tamaño de la unidad de transmisión
    
# Listar redes VPC
gcloud compute networks list
```

- Los proyectos pueden contener multiples redes VPC.
  - Los nuevos proyectos comienzan con una red por defecto (una red VPC automatica) que tiene una sola subred en cada Región.

- Modo Auto vs Custom
  - Auto: se crea automaticamente una subred por cada región
  - Custom: ninguna subred es creada automaticamente

- ⭐ Cuántas redes VPC se puede crear? - 5 por defecto

{{< gist enmanuelmoreira a80e416ec4be59b5200bc4807fd96162 >}}

### Cuotas

Algunos números desde la [doc](https://cloud.google.com/vpc/docs/quota#limits)

- Cantidad máxima de rangos de IP secundarios por subred - 30
- Cantidad máxima de conexiones a una sola red de VPC - 25
- Cantidad máxima de instancias MV - 15,000 por red

### Cloud Interconnect

- Cloud Interconnect extiende la red local a la red de Google a través de una conexión de latencia baja con alta disponibilidad. - [docs](https://cloud.google.com/network-connectivity/docs/interconnect)

- Nota: se puede contectar a GCP de tres maneras - [docs](https://cloud.google.com/network-connectivity/docs/how-to/choose-product)
  - Cloud VPN
    - ⭐ Cloud VPN es una conexión híbrida
      - Durante su configuración, puede especificar la puerta de enlace de Google Compute Engine VPN

  - Cloud Interconnect
  - Cloud Router
    - Es un servicio Google Cloud totalmente distribuido y administrado que utiliza el Protocolo de Puerta de Enlace Fronteriza ([BGP](https://es.wikipedia.org/wiki/Border_Gateway_Protocol)) para anunciar rangos de direcciones IP

- Para acceder solo a Google Workspace o a las API de Google compatibles:
  - Direct Peering
    - conexión de intercambio de tráfico directa entre la red perimetral y la de Google.
  - Carrier Peering
    - se contrata a un proveedor para obtener servicios de red de nivel empresarial que conectan la infraestructura interna con la Google.
- Otras conexiones
  - CDN Interconnect
    - proveedores de redes de distribución de contenido (CDN) de terceros para establecer vínculos de intercambio de tráfico directos con la red perimetral de Google
  - Niveles de Servicio de Red (Network tier)
    - Se puede especificar cual red debe ser usada las conexiones - [doc](https://cloud.google.com/network-tiers/#tab1)
      - Despues de seleccionar un nivel por defecto, puede ser cambiado en tiempo de despligue
    - Two kind of tiers
      - Nivel Premium: utiliza la red de alta disponibilidad de Google
      - Nivel Standard: más económico, utiliza redes de Internet estándar

### Equilibrador de Carga (Load Balancer)

- [Eli5](https://www.reddit.com/r/explainlikeimfive/comments/1o5i6l/eli5_load_balancing_how_does_it_work_any/) Load Balancer - sirve la petición a las instancias menos ocupadas o con menos tráfico

- Permite ubicar tus recursos detrás de una sola dirección IP que sea accesible de manera externa o interna para la red de nube privada virtual (VPC) - [gcp](https://cloud.google.com/load-balancing/docs/)

- Anycast = la dirección IP de destino tiene múltiples rutas o a dos o más destinos - [wiki](https://es.wikipedia.org/wiki/Difusi%C3%B3n_por_proximidad)

- CLI
  
```bash
# Cree una regla de reenvío para dirigir el tráfico de red a un equilibrador de carga
gcloud compute forwarding-rules create
```

### Escogiendo un Equilibrador de Carga

Basado en los [docs](https://cloud.google.com/load-balancing/docs/choosing-load-balancer)

- Equilibrador de Carga Interno
  - Distribuye el tráfico a instancias dentro de Google Cloud.  
  - Escogiendo basado en el tipo de Tráfico
    - Equilibrador de Carga Interno HTTP(S) - [docs](https://cloud.google.com/load-balancing/docs/l7-internal)
      - Regional
    - Internal TCP/UDP Load Balancing - [docs](https://cloud.google.com/load-balancing/docs/internal)
      - Regional

- Equilibrador de Carga Externo
  - Distribuye el tráfico proveniente desde Internet hacia la VPC
  - Escogiendo basado en zonas y el tipo de tráfico:
    - Si el tráfico es UDP: usar TCP/UDP Externo
  - HTTP(S) Externo - [docs](https://cloud.google.com/load-balancing/docs/https)
    - Global
  - Proxy SSL - [docs](https://cloud.google.com/load-balancing/docs/ssl)
    - Global
  - Proxy TCP - [docs](https://cloud.google.com/load-balancing/docs/tcp)
    - Global
  - Redes Externas TCP/UDP - [docs](https://cloud.google.com/load-balancing/docs/network)
    - Regional

- Nota:
- Secure Sockets Layer
  - SSL opera directamente encima del protocolo de control de transmision (TCP) - [fuente](https://www.websecurity.digicert.com/security-topics/what-is-ssl-tls-https) - [eli5](https://www.reddit.com/r/explainlikeimfive/comments/jsq3m/eli5_what_are_online_security_certificates_ssl/)
  - SSL puede usar TCP [1] para transportar registros SSL, de esta manera, SSL se basa en TCP como servicio - [fuente](https://crypto.stackexchange.com/questions/53786/why-is-ssl-on-top-of-tcp)
- Verificaciones de Estado (Health Check)
  - El Equilibrador de carga puede utilizar mecanismos de verificación de estado - [docs](https://cloud.google.com/load-balancing/docs/health-checks)

### Firewall

- ⭐ Cada VPC implementa un firewall virtual distribuido - [doc](https://cloud.google.com/docs/enterprise/best-practices-for-enterprise-organizations#firewall-rules)

- permite habilitar o denegar conexiones desde o hacia instancias MV - [docs](https://cloud.google.com/vpc/docs/firewalls)
  - Debe especificar y aplicar reglas de entrada (ingress) o salida (egress), no ambas

- Cada red tiene implicitas dos reglas de Firewall que permiten conexiones salientes y bloqueando las conexiones entrantes.

- ⭐ Reglas por Defecto: [doc](https://cloud.google.com/vpc/docs/firewalls#more_rules_default_vpc)
  - Permiten conectarse entre MVs de la misma red y enviar paquetes ICMP

- ⭐ Tráfico que siempre está bloqueado - [doc](https://cloud.google.com/vpc/docs/firewalls#blockedtraffic)
  - Tráfico de salida al puerto de destino TCP 25 (SMTP)
    - “El puerto 25 TCP es frecuentemente bloqueado por Proveedores de Servicio de Internet (ISPs), como una técnica anti-spam, ya que se utiliza en spam de MX y abuso de máquinas de retransmisión/proxy abierto..” - [web](https://help.dreamhost.com/hc/es/articles/217071167-Port-25-Blocking)
  - Protocolos distintos de TCP, UDP, ICMP, IPIP, AH, ESP, SCTP y GRE para direcciones IP externas de los recursos de Google Cloud

- CLI

```bash
# Crea una regla de firewall para Compute Engine
gcloud compute firewall-rules create [NOMBRE_REGLA] [--network=NOMBRE_SUBRED] --destination-ranges[RANGO_CIDR] [--direction]
```

### Cloud Armor

- Ayuda a proteger las aplicaciones y sitios web contra los ataques web y de denegación del servicio. - [doc](https://cloud.google.com/armor)
- Protección DDoS, soporte híbrido y multinube, acceso basado en IP y geo-localización, protección adaptativa (modelo de aprendizaje automático personalizado entrenado)

## 🎒 Almacenamiento

### Tipos de Almacenamiento

**[Documentación de Google Cloud](https://cloud.google.com/compute/docs/disks)**

- **Block storage**
  - Is the traditional storage type for Vm - [netapp](https://cloud.netapp.com/blog/object-storage-block-and-shared-file-storage-in-google-cloud)
    - Stores chunk of raw data linearly in constant size blocks
  - Solución en GCP = [Persistent disk](https://cloud.google.com/persistent-disk), [SSD Local](https://cloud.google.com/local-ssd)

- **Object storage**
  - Permite almacenar y recuperar cualquier cantidad de datos en todo el mundo y en cualquier momento - [docs](https://cloud.google.com/storage/docs)
    - Vincula un enlace (por ejemplo, una URL) a un objeto específico sin jerarquia - [reddit](https://www.reddit.com/r/explainlikeimfive/comments/4ifybx/eli5_object_vs_block_vs_file_storage/)  
  - Solución en GCP = [GCS](https://cloud.google.com/storage)

- **Cache**
- Los datos se almacenan en memoria de rápido acceso. Los datos son borrados con el reinicio de la MV (si no se almacenan en otro lugar diferente), pueden sufrir [invalidación de caché](https://en.wikipedia.org/wiki/Cache_invalidation)
- Soluciones en GCP = RAM - [SSD Local](https://cloud.google.com/compute/docs/disks) - [Memorystore](https://cloud.google.com/memorystore)

### Servicios de Almacenamieno de GCP

#### Cloud Datastore / Firestore

- Base de datos documental NoSQL altamente escalable, transaccional, terabytes+
- ⭐ Firestore ****es la nueva versión de Datastore
- Acelera el desarrollo de aplicaciones moviles, web e IoT con conectividad directa a la base de datos - [doc](https://cloud.google.com/firestore)
- ⭐ Datastore usa el lenguaje GQL - [doc](https://cloud.google.com/datastore/docs/reference/gql_reference)
- CLI

```bash
  # Para exportar todos los tipos en el namespace ejemploNs del proyecto exampleProject al bucket exampleBucket
gcloud datastore export gs://exampleBucket --namespaces='exampleNs' --project='exampleProject'
```

#### Filestore - [doc](https://cloud.google.com/filestore)

- Sistema de Archivos en Red (NFS) Gestionado - [docs](https://cloud.google.com/filestore/docs)
  - Permite a un computador cliente acceder a archivos sobre una red casi como si se estuviera acceciendo desde un almacenamiento local - [wiki](https://es.wikipedia.org/wiki/Network_File_System)
- ⭐ Filestore vs GCS
  - “(filestore) ofrece almacenmaiento de archivos de alto rendimiento a aplicaciones que se estén ejecutando en instancias Compute Engine y Kubernetes Engine” - [stackoverflow](https://stackoverflow.com/a/52035952/5099361)

#### Memorystore - [doc](https://cloud.google.com/memorystore)

- Basicamente, es un servicio redis gestionado.
- Reduce la latencia con un servicio en memoria escalable, seguro y con alta disponibilidad para Redis y Memcached.
  - 100% compatible Redis y Memcached de código abierto
- ⭐ Tamaño máximo: 300 GB - [doc](https://cloud.google.com/memorystore/docs/redis/redis-overview?hl=it)
- CLI - [doc](https://cloud.google.com/memorystore/docs/redis/quickstart-gcloud)

```bash
# Crea una instancia Memorystore para Redis:
gcloud redis instances create myinstance --size=2 --region=us-central1 \
    --redis-version=redis_5_0
```

#### BigTable

- Es un servicio de base de datos NoSQL escalable y completamente administrado para grandes cargas de trabajo de estadísticas y operaciones con una disponibilidad de hasta el 99.999%
- baja latencia, no transaccional, almacenamiento de columna ancha, sin consultas tipo SQL, expone la API de Apache HBase, petabytes+
- Estructura
  - Instancia - [doc](https://cloud.google.com/bigtable/docs/creating-instance)
    - Una instancia de Cloud Bigtable es un contenedor para hasta cuatro clústeres de Bigtable.
    - Las instancias puede tener uno o mas clústeres, localizados en diferentes zonas
  - Clústeres - [doc](https://cloud.google.com/bigtable/docs/instances-clusters-nodes#clusters)
    - Servicio Bigtable service en un lugar especifico
  - Nodos - [doc](https://cloud.google.com/bigtable/docs/instances-clusters-nodes#nodes)
    - Recursos de computo que Bigtable utiliza
    - Cada clúster de una instancia tiene 1 o más nodos
- CLI - [docs](https://cloud.google.com/bigtable/docs/cbt-reference)
  
{{< gist enmanuelmoreira beaa435c8fda42d2991f10dca7dd5952 >}}

#### Cloud Storage (gcs)

- Los Objetos son inmutables, versionado de objetos**,** petabytes+
- Tipo de acceso: Uniforme (recomendado), granular (usa ACL obsoletas) - [doc](https://cloud.google.com/storage/docs/access-control)
- GCS - Buenas Práticas - [doc](https://cloud.google.com/storage/docs/best-practices)
- Uso compartido y colaboración  - [doc](https://cloud.google.com/storage/docs/collaboration)
- ⭐ URLs Firmadas
  - Es una URL que proporciona permisos y tiempo limitados para realizar una solicitud - [doc](https://cloud.google.com/storage/docs/access-control/signed-urls)
    - “permitiendo a los usuarios sin credenciales a realizar una tarea específica en un recurso”
  - Cada URL Firmada esta asociada a una Cuenta de Servicio
  - La petición mas común para URLs Firmadas son las de subir y descargar objetos

- Clases de Almacenamiento - [doc](https://cloud.google.com/storage/docs/storage-classes)
  - Opciones Cold:
    - Nearline Storage: lectura o modificación en promedio 30 días o menos
    - Coldline Storage: lectura o modificación una vez cada 90 días
    - Archive Storage: una lectura o modificación en 365 días

  - ⭐ Cambiar clase de Almacenamiento
    - Nota: es un proceso de reescritura, no se puede cambiar la clase de almacenamiento original del bucket
    - ⭐ Nota: se utiliza para esto el comando gsutil, no gcloud
      - El comando gsutil solo puede ser utilizado para Cloud Storage. - [stackoverflow](https://stackoverflow.com/a/58147001)

```bash
# Reescribe un objeto origen a un objeto desitno .
gsutil -m rewrite -s coldline gs://bucket/**
        
# Crea un bucket
gsutil mb gs://NOMBRE_BUCKET
```

- Localización
  - Región: Baja latencia dentro de una sola región
  - Multi-región: Alta disponibilidad a través de una mayor área
  - Regiones-Duales: Alta disponibilidad y baja latencia a través de 2 regiones
  - ⭐ No hay una manera única de mover objetos desde Región a Multi-Región - [stackoverflow](https://stackoverflow.com/a/51508228)

- ⭐ Versionado
  - Cuando se usa versionado, la última versión del objeto es llamado objeto "vivo" - [doc](https://cloud.google.com/storage/docs/using-object-versioning#setup)

#### Cloud SQL

- Base de Datos Relacional SQL, transaccional, servicio de réplicas, terabyte+
- MySQL, PostgreSQL, y (MS) SQL Server
- ⭐ CLI

```bash
# Actualizar la configuración de una instancia Cloud SQL
gcloud sql instances patch [NAME] [--backup-start-time] [--backup-location] 
    
# Comandos para trabajar con respaldos de las instancias Cloud SQL
gcloud sql backups [create/delete/describe/list/restore]
```

#### Cloud Spanner

- Base de Datos Relacional SQL, escalado horizontal, petabytes+

#### BigQuery

- Datalake almacenamiento de datos (OLAP), análisis de datos
- CLI

```bash
# Importar datos a bq
bq load --autodetect --source_format=FORMAT DATASET.TABLE PATH_TO_SOURCE
```

## 🧠 Servicios de Cómputo

### Compute engine (VM)

- IaaS - MV bajo demanda
- IP address - [doc](https://cloud.google.com/compute/docs/instances/view-ip-address)
  - Para localizar la IP externa (e interna) de la MV, se debe utilizar: ```gcloud compute instances list```
  - Si la MV está autorizada, puede recibir una dirección IP externa y mapearla a su dirección interna

- ⭐ Cuotas [doc](https://cloud.google.com/compute/quotas#checking_regional_quota)
- Hay dos tipos de Cuotas: proyecto o regional
  - Proyecto: 
    - Puestas para un proyecto especifico, se puede saber con: ```gcloud compute project-info describe --project ID_PROYECTO```
  - Regional: 
    - Las cuotas de las MV son gestionada a nivel regional: ```gcloud compute regions describe REGION```

- Note:
  - “Las cuotas no garantizan que los recursos estén disponibles siempre”

- Almacenamiento:
  - SSDs Local - [doc](https://cloud.google.com/compute/docs/disks/local-ssd)
    - Mejor rendimiento: están fisicamente conectados al servidor que hospeda la instancia de MV
    - 375 GB de capacidad
    - Pueden ser conectados 24 discos para un total máximo de 9 TB
- scripts de apagado - [doc](https://cloud.google.com/compute/docs/shutdownscript)
  - tienen una limitada cantidad de tiempo para finalizar los procesos corriendo (Instancias descartables: 30s)[script](https://cloud.google.com/compute/docs/shutdownscript#provide_shutdown_script_contents_directly) desde la Consola usando la clave de metadatos shutdown-script
- Instance groups
  - colección de MV que pueden ser gestionados como una simple entidad - [doc](https://cloud.google.com/compute/docs/instance-groups)
  - Hay dos Tipos de IG:
    - Grupo de Instancias Gestionadas (MIGs)
      - ⭐ Múltiples e identicas MV, escalables y altamente disponibles.
      - crea MV desde un template de instancias y configuración Stateful opcional (por ejemplo, discos)
      - Hay dos tipos de MIG:
        - MIG Zonal
          - despliga instancia en una zona
        - MIG Regional
          - despliga instancias a través de multiples zonas de la misma Región
    - Grupo de Instancias No Gestionadas
      - pueden contener instancias heterogeneas, necesita ser gestionado por un humano
      - no ofrecen autoescalado, autosanado, soporte a actualizaciones rolling ni soporte multi-zone
      - ⭐ "Usa grupo de instancias no gestionadas cuando se necesiten aplicar equilibrio de carga a grupos de instancias heterogeneas, o si se necesita gestionar las instancias por un humano" - [doc](https://cloud.google.com/compute/docs/instance-groups#unmanaged_instance_groups)
- Ahorra Costos
  - Instancias de MV Interrumpibles (Preemptible)
    - ⭐ Instancias de MV Interrumpibles no pueden ser migradas a una instancia de MV regular - [doc](https://cloud.google.com/compute/docs/instances/preemptible)
  - Descuentos por Comprometimiento de Uso
    - Si hubiere una carga de trabajo que siempre (o la mayor parte de las veces) sea requerida, se puede comprometer a utilizar una MV de 1 a 3 años y recibir un descuento.
- MV Protegida - [doc](https://cloud.google.com/security/shielded-cloud/shielded-vm)
  - ⭐ ofrece integridad verificable de tus instancias de VM de Compute Engine, para que puedas estar seguro de que las instancias no se expusieron a software malicioso o rootkits de nivel de inicio y kernel
  - uso del Inicio seguro, el inicio medido habilitado para el módulo de plataforma segura virtual (vTPM) y el monitoreo de integridad.
- Cloud Console
  - ⭐ Para reiniciar una MV, se debe utilizar el botón **reset**
  - Se pueden ordenar las instancias de MV por Etiquetas, estatus, zonas, usado por, IP, etc.
  - ⭐ Se puede filtrar una instancia por You can filter VM instances by Etiquetas, estatus, Miebro del Grupo de Instancias Gestionadas, IP, Propiedades de la MV.
- GPU
  - ⭐ Se debe configurar las instancias de GPU para que se detengan durante los eventos de mantenimiento del host - [doc](https://cloud.google.com/compute/docs/gpus/gpu-host-maintenance)
- Snapshot
  - ⭐ Compute Engine usar instantaneas incrementales por lo que cada instantea contiene solo los datos que han sufrido cambios desde la instantanea previa. - [doc](https://cloud.google.com/compute/docs/disks/create-snapshots)
  - Para tomar una instantanea se debe tener el rol de **Compute Storage Admin** - [doc](https://cloud.google.com/compute/docs/access/iam#compute.storageAdmin)
    - Permisos para crear, modificar y eliminar discos, imagenes e instantaneas.
- Discos
  - Los discos están a nivel regional, por lo que se puede habilitar replicación regional
    - Regional: el disco será reolicado sincronicamente a través de dos zonas dentro de una Región
  - Caso de Uso: Copiar una MV desde una zona a otra en la misma Región
    - gcloud para copiar el disco a una nueva zona, para entonces crear una MV a partir de ese disco
    - Otros: Mover una instancia entre zonas - [doc](https://cloud.google.com/compute/docs/instances/moving-instance-across-zones)

### GKE - Google Kubernetes Engine

- Ejecuta aplicaciones contenereizas en un entorno gestionado
- construido sobre Compute engine
- Clúster Regional
  - Se puede especificar un clúster, y GCP replica las configuraciones a todas las demás zonas
  - Problema: se paga por recursos multiplicando el número de zonas disponibles
- Clúster Multizona
  - Se puede escoger mas de una zona de una Región
  - Se reducen costos (por ejemplo se pueden seleccionar 2 zonas en lugar de 3)
    - Y no es necesario mantener los mismos nodos en ambas zonas, en caso de fallas en una zona, esto puede ser un potencial
  - Problema: el nodo maestro está solo en la zona principal Si la zona muere, el nodo maestro muere
- Aprovisionamiento Automático - [doc](https://cloud.google.com/kubernetes-engine/docs/how-to/node-auto-provisioning)
  - GCP tratará de entender los recursos requeridos por un pod, y creará un pool de nodos bajo demanda con los suficientes recursos para alojar ese pod
  - Adapta el pool de nodos de acuerdo a los requisitos que necesite el pod
- Binary Authorization
  - Despliega solo contenedores verificados en Google Kubernetes Engine.

### Cloud Run
  
- En una frase: el usuario le proporcionará a Google Cloud Run un contenedor Docker que contebga un servidor web.
  - Google ejecutará este contenedor y creará un endpoint HTTP. - [medium](https://medium.com/codeduck/cloud-run-vs-app-engine-dc1871abedca)
- ⭐ Puede ser facilmente confundido con App engine (particularmente con App Engine Flex)
  - [aquí] hay algunos comentarios utiles:
    - “AppEngine solo puede ser desplegado en una sola Región.”
    - Cloud run permite desplegar un servicio a cualquier Región dentro de un proyecto haciendo su API realmente global, todo dentro del mismo proyecto.

### App Engine

- ⭐ Definiciones:
  - Una aplicación por proyecto
  - La aplicación puede tener multiples Servicios:
    - Los componentes lógicos pueden compartir de forma segura las funciones de App Engine y comunicarse
  - Cada cambio en el Servicio crea una nueva Versión
  - Cada Versión se ejecuta en una máquina llamada Instancia
    - Instancias Residentes:
      - Cada vez que es escalado a 0, esas están siguen igual en linea o vivas. - [medium](https://medium.com/google-cloud/app-engine-resident-instances-and-the-startup-time-problem-8c6587040a80)
    - Instancias Dinámicas:
      - crea instancias con escalado automático - [doc](https://cloud.google.com/appengine/docs/standard/java/how-instances-are-managed#scaling_dynamic_instances)
- PaaS - Ejecuta codigo en la nube sin preocuparnos por la insfraestructura
  - Se le indica a Google como la aplicación debe ser ejecutada.
  - App Engine creará y ejecutará un contenedor con esas instrucciones.
  - Por ejemplo, especificar un app.yml con:
    - runtime: nodejs12
    - entrypoint: node build/server.js

- Caracteristicas Básicas
- No se puede escribir en el disco local, se debe probar la aplicación localmente, para que encaje bien con la arquitectura de microservicios
- ⭐ Solo una App Engine por Proyecto - [doc](https://cloud.google.com/appengine/docs/flexible/nodejs/an-overview-of-app-engine#components_of_an_application)
- ⭐ Limites - [doc](https://cloud.google.com/appengine/docs/flexible/nodejs/an-overview-of-app-engine#limits)
  - Servicios máximos por aplicación: 5
  - Versiones máximas por aplicación: 5
  - Instancias máximas por versión con escalado manual: 20
- Entornos - [docs](https://cloud.google.com/appengine/docs/the-appengine-environments)
  - Entorno Estandar
    - código en una versión especifica en un lenguaje de programación especifico, inicio rápido (sec)
  - Entorno Flexible
    - ofrece docker, inicio más lento (min)
- ⭐ Localizaciones
  - App Engine es un servicio regional, No se pueden trasladar aplicaciones de Región. - [doc](https://cloud.google.com/appengine/docs/locations)
- Servicios
  - Se pueden desplegar multiples servicios en un solo App Engine dentro del mismo proyecto en que la reside
  - “Una aplicación de App Engine consta de un solo recurso de aplicación que consiste en uno o más servicios.” - [doc](https://cloud.google.com/appengine/docs/standard/nodejs/an-overview-of-app-engine)
  - “Dentro de cada servicio se implementan sus versiones”
- Limites
  - Cantidad de Servicios máximos por aplicación - App Grátis 5 - App de Pago 105
  - Cantidad de Versiones máximas por aplicación - App Grátis 15 - App de Pago 210

{{< gist enmanuelmoreira 7ba177dfdd6aa1f63ffa48c6f5317066 >}}

- Si se desea dividir el tráfico con una cookie, el nombre de la cookie debe ser GOOGAPPUID - [doc](https://cloud.google.com/appengine/docs/flexible/python/splitting-traffic#cookie_splitting)

- ⭐ Escalado - [doc](https://cloud.google.com/appengine/docs/standard/python/how-instances-are-managed#apps_with_basic_scaling)
  - Escalado Básico**:**
    - App Engine intenta de mantener bajos los cotos, aún cuando esto resulte en una alta latencia y el volumen de petición se incremente
  - Escalado Automático:
    - Cada instancia en la aplicacion tiene su propia cola para peticiones entrantes. App Engine automáticamente gestiona el incremento en la carga

- ⭐ Instancia
  - La clase de instancia determina la cantidad de memoria y CPU disponibles para cada instancia - [doc](https://cloud.google.com/appengine/docs/standard) (el tipo de MV)

- ⭐ HTTP url - [doc](https://cloud.google.com/appengine/docs/flexible/python/how-requests-are-routed)
  - https://ID_PROYECTO.ID_REGION.r.appspot.com

- app.yaml - [doc](https://cloud.google.com/appengine/docs/standard/python/config/appref)
  - Se puede configurar la app de App Engine en el archivo app.yaml.
  - Algunas opciones interesentes que deben estar en el archivo yaml son:
    - api_version: Obligatorio
    - default_expiration: Establece un período de caché predeterminado global para todos los controladores de archivos estáticos de una aplicación.
    - env_variables: Se definen variables de entorno
    - includes: permite incluir el archivo de configuración de cualquier biblioteca o servicio en toda la aplicación.
    - instance_class
    - libraries: - Obsoleto, usar requirements.txt para especifiar una biblioteca Python
    - threadsafe: Obligatorio, Configura la aplicación para utilizar peticiones concurrentes
    - version: - mejor configurarlo con la CLI
    - automatic_scaling
      - [min/max]_instances
      - ⭐ max_concurrent_requests: número de peticiones concurrentes que una instancia de escalado automático puede aceptar antes de que el programador genere una nueva instancia (predeterminado: 10, máximo: 80).
      - max_idle_instances: cantidad máxima de instancias inactivas que App Engine debe mantener para esta versión.
    - basic_scaling
      - max_instances: ⭐ Nota: el valor min_instances no existe!
      - idle_timeout: La instancia se cerrará en esta cantidad de tiempo después de recibir su última petición.

## 🖇️ Recursos

- ⭐ Google developer cheat sheet - [github](https://github.com/gregsramblings/google-cloud-4-words/blob/master/Poster.pdf)

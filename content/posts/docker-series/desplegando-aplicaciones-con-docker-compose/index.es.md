---
title: "Desplegando Aplicaciones con Docker Compose"
date: 2021-09-30
lastmod: 2021-09-30
authors: [enmanuelmoreira]
description: "En artículos anteriores hemos usado Docker de manera imperativa, es decir, a través de la linea de comandos ejecutando las ordenes y sus argumentos (docker images, docker ps, docker run, docker exec`, etc); cuando mucho hemos trabajado con uno o dos contenedores. En este artículo vamos a configurar nuestras aplicaciones de manera mas simple con Docker Compose"
draft: false

resources:
- name: "featured-image"
  src: "featured-image.png"

categories: ["Linux","Contenedores"]
tags: ["docker","contenedores","infraestructura"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

## Introducción

***

En artículos anteriores hemos usado Docker de manera imperativa, es decir, a través de la linea de comandos ejecutando las ordenes y sus argumentos (`docker images`, `docker ps`, `docker run`, `docker exec`, etc); cuando mucho hemos trabajado con uno o dos contenedores.  

## El Problema

***

Imaginemos la siguiente situación: queremos desplegar un Wordpress por ejemplo, el cual consta de la aplicación como tal y el Servidor de Base de Datos al que debemos pasarle unos parámetros como el usuario root, la contraseña el puerto, etc. Entonces nos quedaría algo así:

```bash
docker run --name wordpress -e WORDPRESS_DB_HOST=10.1.2.3:3306 \
           -e WORDPRESS_DB_USER=usuario_db \
           -e WORDPRESS_DB_PASSWORD=password_db -d wordpress
```

Y para crear la base de datos:

```bash
docker run -p 127.0.0.1:3306:3306  --name mariadb_server \
           -v /directorio/de/datos/:/var/lib/mysql
           -e MARIADB_ROOT_PASSWORD=password_db -d mariadb
```

Se hace muy engorroso tener que recordar todos los parámetros necesarios para configurar dos servicios diferentes (Wordpress y MariaDB) al mismo tiempo. Así mismo, tendríamos que saber que dirección IP tendría el contenedor de MariaDB ya que como recordarás en el articulo **[Conceptos y Comandos Básicos en Docker](https://www.itsimplenow.com/conceptos-y-comandos-basicos-en-docker/)** los contenedores son efimeros, y una vez que mueren, se reinician o fallan, esa dirección IP se pierde y al levantar nuevamente el contenedor, se asigna otra diferente.

## Docker Compose

A través de un archivo en formato `.yaml`, podemos realizar toda nuestra configuración **Declarativamente** de manera que vamos a obtener cierto orden en nuestros servicios. Regularmente el archivo lo ubicamos en un directorio aparte que puede contener los datos que vamos a guardar y los archivos de confguracion que le podemos cargar a nuestros servicios (un nginx.conf, my.cnf, etc)  

El formato `YAML` es el mismo usado para Ansible y Kubernetes, consta de una serie de claves y valores que deben ser identados con dos espacios o con un TAB. Una de las desventajas de utilizar YAML es que ese identado suele ser bastante problematico con configuraciones grandes, sin embargo, no hay nada que un buen plugin de de VSCode o VSCodium no puedan manejar.  

## Instalando Docker Compose

### Linux

Es relativamente fácil instalar Docker Compose en Linux, todo lo que debemos hacer es ejecutar el siguiente comando en una terminal:  

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

En distribuciones como Manjaro o ArchLinux, ya lo tendremos disponible en los repositorios, es solo ejecutar:

```bash
sudo pacman -S docker-compose 
```

### Microsoft Windows y MacOS

Para instalar Docker Compose en ambos sistemas operativos, debemos instalar Docker Desktop, el cual es solo descargar el instalador **[Windows](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)** | **[MacOS con Procesadores Intel](https://desktop.docker.com/mac/main/amd64/Docker.dmg?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-mac-amd64)** | **[MacOS con Procesadores ARM](https://desktop.docker.com/mac/main/arm64/Docker.dmg?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-mac-arm64)** y seguir las instrucciones del instalador.

Nos aseguramos que Docker Compose esté instalado ejecutando:

```bash
docker-compose --version
```

```plaintext
docker-compose version 1.29.2, build unknown
docker-py version: 5.0.2
CPython version: 3.9.7
OpenSSL version: OpenSSL 1.1.1l  24 Aug 2021
```

## Estructura del Archivo docker-compose.yaml

Consta de tres llaves básicas:  

`version: "3"`: aquí declaramos la versión de Docker Compose que queremos utilizar.

`service`: aquí declaramos todos los contenedores que forman parte de nuestro servicio/aplicación. Le podemos colocar los nombres de los mismos.

`networks` (opcional): aquí declaramos el driver de red que usarán nuestros servicios. Recordemos que en Docker tenemos los drivers: **bridge** (es el driver por defecto. Los contenedores que la usen van a obtener una IP interna y se van a poder comunicar entre ellas, también en el host), **host** (toma la dirección IP directamente de nuestro Router), **Macvlan** (el contenedor dentro de esta red, se le va a asignar una dirección MAC única y además va a estar en la red real, pudiendo acceder a los recursos de la misma (DHCP, DNS, etc)), **null** (El contenedor que se encuentre en esta red, no tendrá IP. Es útil en aquellos casos donde se ejecuten tareas o scripts en el cual el resultado deba ser almacenado en un volumen)

Retomando el ejemplo anterior (Wordpress + MariaDB), el archivo docker-compose.yaml quedaría de la siguiente manera:

```yaml
version: '3.1'
services:
  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: root
      WORDPRESS_DB_PASSWORD: password
      WORDPRESS_DB_NAME: wordpress
    volumes:
      - $PWD/wordpress/:/var/www/html
    depends_on:
      - db
  db:
    image: mariadb
    restart: always
    environment:
      MYSQL_DATABASE: wordpress
      MYSQL_USER: root
      MYSQL_ROOT_PASSWORD: password
    volumes:
      - $PWD/db/:/var/lib/mysql
```

Desglosemos uno a uno los parámetros:  

* `image: wordpress`: la imagen que vamos a usar.  
  
* `restart: always`: con este parámetro nos aseguramos que se reinicie el contenedor siempre así haya un error con el mismo. También podemos hacer que el contenedor se inicie automáticamente al reiniciar el sistema.  
  
* `ports`: **puerto del host:puerto del contenedor**.  El guión significa que es un parámetro de tipo lista, por lo que podemos redireccionar más puertos si fuese necesario.  

* `environment:`: aquí colocamos las variables de entorno que necesiamos para que ambos servicios funcionen. Si observas bien, hay varios parámetros como `WORDPRESS_DB_HOST`, `WORDPRESS_DB_USER`,  `WORDPRESS_DB_PASSWORD` y `WORDPRESS_DB_NAME` que la imagen de Wordpress necesita para poder conectarse a la base de datos.  Hay un detalle a considerar con el parámetro `WORDPRESS_DB_HOST: db` es que **db** es el nombre que le colocamos al servicio que se ocupa de levantar la base de datos MariaDB con lo va a hacer match con esa etiqueta, permitiendonos conectarnos a la db sin necesidad de colocar la dirección IP de la base de datos.  

* `depends_on`: con este parámetro le decimos especificamente al servicio Wordpress que espere a que el servicio db esté iniciado antes de levantar el Wordpress.  Es útil cuando tenemos un servicio que tarda un poco en iniciar (normalmente aplicaciones Java o de base de datos)  

* `volumes`: con $PWD estamos mapeando nuestro directorio actual (donde se encuentra el docker-compose.yaml) al directorio del contenedor donde están los archivos de datos. En el caso de MariaDB redirecciona el directorio db del host a /var/lib/mysql del contenedor. Lo mismo con Wordpress

## Iniciando Nuestra Aplicación

Para iniciar nuestra aplicación de ejemplo con Wordpress y MariaDB, debemos estar en el directorio donde se encuentra nuestro docker-compose.yaml y ejecutamos:

```bash
docker-compose up -d
```

Para verificar que Wordpress haya iniciado correctamente, en cualquier navegador colocamos la direccion **<http://localhost:8080>** y voilà.  

![Wordpress](/images/docker-compose/service.png "Wordpress")

Si omitimos el `-d` veremos que nuestro servicio inicia en primer plano y veremos lo que está ocurriendo con las aplicaciones.

Para detener el servicio:

```bash
docker-compose stop
```

Para iniciar solo un servicio en particular:

```bash
docker-compose up -d db
```

Para detener un servicio en particular:

```bash
docker compose stop wordpress
```

Para ver los contenedores en ejecución:

```bash
docker-compose ps
```

Para entrar a un servicio y ejecutar comandos:

```bash
docker-compose exec db bash
```

Para ver las imagenes utilizadas:

```bash
docker-compose images
```

Si queremos actualizar las imagenes de los contenedores sin interrumpir el servicio:

```bash
docker-compose pull
```

Para eliminar todo:

```bash
docker-compose rm
```

Nos vemos en la próxima! Happy Dockering :smile:

## Referencias

***

* <https://docs.docker.com/engine/reference/builder/>
* <https://docs.docker.com/compose/profiles/>

## Artículos sobre Docker

***

* [Como Instalar Docker en Linux](https://www.itsimplenow.com/como-instalar-docker-en-linux/)
* [Como Instalar Portainer: El Mejor Gestor Gráfico de Docker en Linux](https://www.itsimplenow.com/como-instalar-portainer-el-mejor-gestor-grafico-de-docker-en-linux/)  
* [Conceptos y Comandos Básicos en Docker](https://www.itsimplenow.com/conceptos-y-comandos-basicos-en-docker/)
* [Construyendo Imágenes Personalizadas en Docker con Dockerfile](https://www.itsimplenow.com/construyendo-imagenes-personalizadas-en-docker-con-dockerfile)
* Desplegando Aplicaciones con Docker Compose
* [Como Configurar un Registro Privado de Docker en Linux](https://www.itsimplenow.com/como-configurar-un-registro-privado-de-docker-en-linux)
* [SupervisorD: Gestionando Procesos en Docker](https://www.itsimplenow.com/supervisord-gestionando-procesos-en-docker/)
* [Buenas Prácticas al Escribir Dockerfiles](https://www.itsimplenow.com/buenas-practicas-al-escribir-dockerfiles/)
* [Crear Imágenes Multi-Arquitectura en Docker con buildx](https://www.itsimplenow.com/crear-imagenes-multi-arquitectura-de-docker-con-buildx/)

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

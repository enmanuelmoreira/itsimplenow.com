---
title: "Buenas Prácticas al Escribir Dockerfiles"
date: 2022-03-10
lastmod: 2022-03-10
authors: [enmanuelmoreira]
description: "En este artículo les voy a hablar de algunos consejos y/o buenas prácticas al momento de escribir Dockerfiles y, de esta manera, evitar problemas de seguridad y optimizar la construcción en nuestras imágenes."
draft: false
series: [docker-series]

resources:
- name: "featured-image"
  src: "featured-image.jpeg"

categories: ["Linux","Contenedores","DevOps"]
tags: ["docker","contenedores","infraestructura","devops"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

***

En este artículo les voy a hablar de algunos consejos y/o buenas prácticas al momento de escribir Dockerfiles y, de esta manera, evitar problemas de seguridad y optimizar la construcción en nuestras imágenes.  

***

## Imágenes root-less

Comenzamos en materia de seguridad y es que recordemos, que a menos que indiquemos lo contrario, los contenedores Docker se ejecutan bajo usuario root, cosa que es muy peligrosa, ya que si un atacante logra comprometer un contenedor, puede tomar control del host. Es por ello que debes agregar usuarios cuando construyas imágenes Docker y así evitar que los contenedores se ejecuten como usuario root:  

```dockerfile
RUN addgroup --system app && add user --system --group app

USER app
```

## COPY vs. ADD

De preferencia usa **COPY** en lugar de **ADD** cuando copies archivos desde el host hacia la imagen. Utiliza ADD solamente cuando:  

* Descargues archivos desde una ubicación remota.
* Extraes un archivo comprimido hacia el destino.

```dockerfile
COPY /ruta/origen /ruta/destino
ADD /ruta/origen /ruta/destino

# Cuando descargamos un archivo externo y lo copiamos al destino
ADD http://www.ruta.ext/archivo.ext /ruta/destino

# Cuando copiamos y extraemos archivos comprimidos desde el host
ADD archivo-comprimido.tar.gz /ruta/destino
```

## Combina Comandos

Combina comandos para reducir al mínimo el número de capas que contendrá la imagen:  

```dockerfile
# 2 comandos, 2 capas:
RUN apt-get update
RUN apt-get install -y apache2

# combinamos 2 comandos en una misma linea, una capa:
RUN apt-get update && apt-get install -y apache2
```

Siempre que sea posible, apoyate del backslash ( \ ) para mejorar la lectura cuando se están instalando muchos paquetes:  

```dockerfile
RUN apt-get update && apt-get install -y \
  bzr \
  cvs \
  git \
  mercurial \
  subversion \
  && rm -rf /var/lib/apt/lists/* 
```

## Cachea Dependencias

Cachea paquetes de Python en el host y móntalos a un volumen (o en su defecto, usa BuildKit):  

```dockerfile
# Utilizando la opción de montar un volumen
-V $HOME/.cache/pip-docker/:/root/.cache/pip

## BuildKit
# syntax = docker/dockerfile:1.2

...

COPY requirements.txt .
RUN --mount=type=cache,target=/root/.cache/pip \
      pip install -r requirements.txt
```

## Limits y Reservations

Limita el uso de CPU y Memoria de tus contenedores para que no sobreconsuman los recursos del host (muy apreciado en aplicaciones Java) y que los demás contenedores en ejecución puedan funcionar de manera normal.  

En la CLI:  

```bash
docker run --cpus=2 -m 512m nginx
```

En Docker Compose:  

```yaml
version: "3.9"
services:
  redis:
    image: redis:alpine
    deploy:
      resources:
        limits:
          cpus: 2
          memory: 512M
        reservations: # Recursos Garantizados al iniciar el contenedor
          cpu: 1
          memory: 256M
```

## Escaneo de Imágenes

Escanea regularmente o cada vez que construyas una imagen, tales como: **[Synk](https://snyk.io/product/container-vulnerability-management/)**, **[Trivy](https://www.aquasec.com/products/trivy/)** o **[Anchore](https://anchore.com/opensource/)**, lo que te permitirá hacer chequeo de vulnerabilidades e informarte de malas prácticas al escribir Dockerfiles. Además te recomiendo **[hadolint](https://github.com/hadolint/hadolint)** para validar la sintaxis en tus Dockerfiles.  

## Firma de Imágenes

Es buena práctica firmar y verificar tus imágenes Docker para prevenir ejecutar imágenes de dudosa procedencia o que hayan sido corrompidos. Para esto agregamos la siguiente variable de entorno:  

```bash
DOCKER_CONTENT_TRUST = 1
```

## Multi-stage Builds

Utiliza multi-stage builds, te va a permitir reducir drasticamente el tamaño de la imagen final de tu aplicación, sin tener que reducir el número de capas intermediarias y archivos.

Utilizaríamos entonces una imagen con todas las dependencias que necesitaramos para poder compilar el proyecto y otra imagen mínima para copiar el ejecutable/componentes de aplicación.  

El siguiente Dockerfile ilustra la construcción y posterior copiado de una aplicación en Go:  

```dockerfile
# syntax=docker/dockerfile:1
FROM golang:1.16-alpine AS build

# Instalamos las herramientas requeridas para compilar la aplicación
# Ejecuta `docker build --no-cache .` para actualizar dependencias
RUN apk add --no-cache git
RUN go get github.com/golang/dep/cmd/dep

# Lista de las dependencias del proyecto en Gopkg.toml y Gopkg.lock
# Esas capas se recontruirán cuando los archivos Gopkg se actualicen
COPY Gopkg.lock Gopkg.toml /go/src/project/
WORKDIR /go/src/project/
# Instalando dependencias
RUN dep ensure -vendor-only

# Copiamos el proyecto completo y construimos
# Esta capa se recontruirá cuando un archivo cambie en el directorio del proyecto
COPY . /go/src/project/
RUN go build -o /bin/project

# Y ahora copiamos el ejecutable hacia una imagen nueva
FROM scratch
COPY --from=build /bin/project /bin/project
ENTRYPOINT ["/bin/project"]
CMD ["--help"]
```

## Credenciales y Confidencialidad

Nunca, nunca coloques secretos o credenciales en Dockerfiles (variables de entorno, argumentos, o credenciales en texto plano en cualquier comando)

Ten cuidado con aquellos archivos que se copian dentro de la imagen. Aún si el archivo es eliminado en alguna instrucción posterior en el Dockerfile, puede ser accesado por las capas anteriores, ya que no está realmente eliminado, sino "escondido" en el sistema de archivos final. Así, que cuando estés construyendo imágenes, toma en cuenta lo siguiente:  

* Si la aplicación soporta configuración vía variables de entorno,  usalas para configurar los secretos en tiempo de ejecución (opción "-e" en docker run), o usa [Docker secrets](https://docs.docker.com/engine/swarm/secrets/), [Kubernetes secrets](https://kubernetes.io/docs/concepts/configuration/secret/) para proveer los valores como variales de entorno.  

* Usa archivos de configuración y **[móntalas](https://docs.docker.com/storage/bind-mounts/)** como volúmenes en docker, o móntalas como secretos de Kubernetes.

Además, tus imágenes no deberían tener ninguna información sensible o confidencial o valores de configuración que muestren algún ambiente de desarrollo (por ejemplo, production, staging, etc.).

Espero les haya gustado y nos vemos en la próxima! Happy Dockering :smile:

## Artículos sobre Docker

***

* [Como Instalar Docker en Linux](https://www.itsimplenow.com/como-instalar-docker-en-linux/)
* [Como Instalar Portainer: El Mejor Gestor Gráfico de Docker en Linux](https://www.itsimplenow.com/como-instalar-portainer-el-mejor-gestor-grafico-de-docker-en-linux/)  
* [Conceptos y Comandos Básicos en Docker](https://www.itsimplenow.com/conceptos-y-comandos-basicos-en-docker/)
* [Construyendo Imágenes Personalizadas en Docker con Dockerfile](https://www.itsimplenow.com/construyendo-imagenes-personalizadas-en-docker-con-dockerfile)
* [Desplegando Aplicaciones con Docker Compose](https://www.itsimplenow.com/desplegando-aplicaciones-con-docker-compose/)
* [Como Configurar un Registro Privado de Docker en Linux](https://www.itsimplenow.com/como-configurar-un-registro-privado-de-docker-en-linux)
* [SupervisorD: Gestionando Procesos en Docker](https://www.itsimplenow.com/supervisord-gestionando-procesos-en-docker/)
* Buenas Prácticas al Escribir Dockerfiles
* [Crear Imágenes Multi-Arquitectura en Docker con buildx](https://www.itsimplenow.com/crear-imagenes-multi-arquitectura-de-docker-con-buildx/)

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

---
title: "SupervisorD: Gestionando Procesos en Docker"
date: 2022-03-08
lastmod: 2022-03-08
authors: [enmanuelmoreira]
description: "¿Alguna vez te has encontrado con el escenario donde necesitas ejecutar más de 1 proceso en un mismo contenedor? A pesar que Docker provee la herramienta Docker Compose para levantar aplicaciones en distintos contenedores, Docker Compose necesita de ser configurado mediante un archivo YAML y poder ejecutar esos servicios. A veces necesitamos (aunque no es lo recomendable), ejecutar más de 2 procesos en un mismo contenedor."
draft: false
series: [docker-series]

resources:
- name: "featured-image"
  src: "featured-image.png"

categories: ["Linux","Contenedores","DevOps"]
tags: ["docker","contenedores","infraestructura","devops","supervisord"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

## Introducción

***

**¿Alguna vez te has encontrado con el escenario donde necesitas ejecutar más de 1 proceso en un mismo contenedor?**  

A pesar que Docker provee la herramienta Docker Compose para levantar aplicaciones en distintos contenedores, Docker Compose necesita de ser configurado mediante un archivo YAML y poder ejecutar esos servicios. A veces necesitamos (aunque no es lo recomendable), ejecutar mas de 2 procesos en un mismo contenedor.  

Recordemos que el objetivo de Docker es precisamente ejecutar un proceso aislado completamente de otro (un contenedor es un proceso del sistema) En este articulo utilizaremos una herramienta que nos permite lograr lo anteriormente expuesto: **SupervisorD.**

## SupervisorD: Por qué y Para qué

Uno de los problemas que enfrentamos al desplegar multiples servicios como SSH, Http es: ¿Cómo controlamos esos servicios? Al no tener scripts de inicio o systemd habilitado dentro de los contenedores, necesitaríamos un mecanismo para iniciar, detener y monitorear el estatus de los mismos.  

![SupervisorD](/images/docker-supervisord/supervisord-0.png "Dos procesos en un contenedor con SupervisorD")

Entonces, **SupervisorD** es una herramienta que nos permite gestionar un número diferente de procesos simultaneamente en Linux. supervisorD necesita de un archivo de configuración .conf donde le especificamos los procesos y las diferentes opciones relaciones para que ese proceso pueda iniciar, auto reiniciarse, la ubicación de los logs, etc.  

Aunque como dije en la introducción, lo recomendado es que por cada servicio haya un contenedor, SupervisorD se puede utilizar en casos donde necesitemos tener otro proceso ejecutándose en el mismo contenedor, como por ejemplo, si tenemos una aplicación que necesite tener Django con Redis.  

Sin embargo, la desventaja principal seria que no podriamos escalar nuestro servicio de manera **[Horizontal](https://es.wikipedia.org/wiki/Escalabilidad#Escalabilidad_horizontal)** (es decir, colocar mas contenedores para que reciban mas tráfico tanto de la aplicación como de Redis), sino que tendría que ser de manera **[Vertical](https://es.wikipedia.org/wiki/Escalabilidad#Escalabilidad_vertical)** (asignar mas recursos de la máquina para que puedan ser consumidas por el contenedor)

## Configurando SupervisorD

En el ejemplo que voy a utilizar, vamos a instalar SSH sobre una imagen preexistente de Apache, y veremos como los dos procesos coexistirán en en mismo contenedor.  

Primero, vamos a crear un Dockerfile con el siguiente contenido:

```dockerfile
FROM httpd:alpine

RUN apk add --no-cache openssh supervisor

RUN ssh-keygen -A

COPY supervisor.conf /etc/supervisor.conf

CMD [ "supervisord", "-c", "/etc/supervisor.conf" ]
```

Como podemos ver, partimos de una imagen de httpd (apache para los entendidos) de la distro alpine (la más livianita que podemos encontrar)  

Luego, vamos a instalar openssh y supervisor, este último para que se encargue del control de los procesos dentro del contenedor a ejecutar.  

Copiamos el archivo de configuración de SupervisorD hacia la imagen y por ultimo el comando a ejecutar cada vez que el contenedor levante, será supervisord y con el argumento "-c" le vamos a decir que cargue el archivo de configuración que acabamos de copiar a la imagen (ubicado en /etc/supervisor.conf)  

El archivo de supervisor.conf debe tener como mínimo el siguiente contenido:  

```bash
[supervisord]
nodaemon=true

[program:sshd]
command=/usr/sbin/sshd -D

[program:apache2]
command=/usr/local/bin/httpd-foreground
```

El primer parámetro significa que no vamos a ejecutar supervisor como un servicio, sino que lo hará en background. En el segundo y tercer parámetro tenemos el comando de arranque de openssh y de apache.

Bueno, ahora lo que tenemos que hacer es construir la imagen Docker:  

```bash
docker build -t supervisor .
```

Una vez construida, vamos a levantar el contenedor:  

```bash
docker run supervisor
```

Como podemos detallar en la siguiente salida (ya que no hemos puesto el contenedor en modo detach), observamos que supervisord inició apache como pid 7 y sshd como pid 8:  

```bash
2022-03-05 20:13:20,222 CRIT Supervisor is running as root.  Privileges were not dropped because no user is specified in the config file.  If you intend to run as root, you can set user=root in the config file to avoid this message.
2022-03-05 20:13:20,224 INFO supervisord started with pid 1
2022-03-05 20:13:21,226 INFO spawned: 'apache2' with pid 7
2022-03-05 20:13:21,228 INFO spawned: 'sshd' with pid 8
2022-03-05 20:13:22,247 INFO success: apache2 entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
2022-03-05 20:13:22,247 INFO success: sshd entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
```

Si ejecutáramos `docker ps` para ver los contenedores que se están ejecutando, veríamos el de supervisor:  

```bash
docker ps
```

```bash
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
eeb1bb74e289   supervisor                 "supervisord -c /etc…"   2 minutes ago   Up 2 minutes   80/tcp    happy_mestorf
```

Vamos a entrar al contenedor, y ver los procesos que está ejecutando:  

```bash
docker exec -it eeb1bb74e289 sh
```

Si hacemos `ps fax`:  

```bash
PID   USER     TIME  COMMAND
    1 root      0:00 {supervisord} /usr/bin/python3 /usr/bin/supervisord -c /etc/supervisor.conf
    7 root      0:00 httpd -DFOREGROUND
    8 root      0:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
   10 www-data  0:00 httpd -DFOREGROUND
   11 www-data  0:00 httpd -DFOREGROUND
   12 www-data  0:00 httpd -DFOREGROUND
   94 root      0:00 sh
  100 root      0:00 ps fax
```

Comprobaríamos entonces que los pid 7 y 8 están ejecutando tanto apache como ssh en el mismo contenedor.  

Esto ha sido todo, espero les haya gustado y nos vemos en la próxima! Happy Dockering :smile:

## Artículos sobre Docker

***

* [Como Instalar Docker en Linux](https://www.itsimplenow.com/como-instalar-docker-en-linux/)
* [Como Instalar Portainer: El Mejor Gestor Gráfico de Docker en Linux](https://www.itsimplenow.com/como-instalar-portainer-el-mejor-gestor-grafico-de-docker-en-linux/)  
* [Conceptos y Comandos Básicos en Docker](https://www.itsimplenow.com/conceptos-y-comandos-basicos-en-docker/)
* [Construyendo Imágenes Personalizadas en Docker con Dockerfile](https://www.itsimplenow.com/construyendo-imagenes-personalizadas-en-docker-con-dockerfile)
* [Desplegando Aplicaciones con Docker Compose](https://www.itsimplenow.com/desplegando-aplicaciones-con-docker-compose/)
* [Como Configurar un Registro Privado de Docker en Linux](https://www.itsimplenow.com/como-configurar-un-registro-privado-de-docker-en-linux)
* SupervisorD: Gestionando Procesos en Docker
* [Buenas Prácticas al Escribir Dockerfiles](https://www.itsimplenow.com/buenas-practicas-al-escribir-dockerfiles/)
* [Crear Imágenes Multi-Arquitectura en Docker con buildx](https://www.itsimplenow.com/crear-imagenes-multi-arquitectura-de-docker-con-buildx/)

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

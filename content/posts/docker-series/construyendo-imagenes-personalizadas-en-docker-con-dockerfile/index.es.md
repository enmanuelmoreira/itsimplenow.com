---
title: "Construyendo Imágenes Personalizadas en Docker con Dockerfile"
date: 2021-09-23
lastmod: 2021-09-23
authors: [enmanuelmoreira]
description: "En artículos anteriores aprendimos qué es Docker, para que nos sirve, algunos comandos básicos, redes, volumenes, etc. Sin embargo, las imágenes que descargamos y probamos están ya definidas por un proveedor o usuario que amablemente las compartió a través de DockerHub, por lo que nos viene la duda: ¿Cómo podríamos contruír nuestras propias imágenes y además de esto, colocar ahi nuestra aplicación para que otros usuarios la prueben?"
draft: false
series: [docker-series]

resources:
- name: "featured-image"
  src: "featured-image.jpeg"

categories: ["Linux","Contenedores"]
tags: ["docker","contenedores","infraestructura"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente botón:  

<a href="https://www.digitalocean.com/?refcode=fc202194cdd9&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge"><img src="https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg" alt="DigitalOcean Referral Badge" /></a>

O a través del siguiente enlace: <https://bit.ly/digitalocean-itsm>

## Introducción

***

En artículos anteriores aprendimos qué es Docker, para que nos sirve, algunos comandos básicos, redes, volumenes, etc. Sin embargo, las imágenes que descargamos y probamos están ya definidas por un proveedor o usuario que amablemente las compartió a través de **[DockerHub](https://hub.docker.com/)**, por lo que nos viene la duda: ¿Cómo podríamos contruír nuestras propias imágenes y además de esto, colocar ahi nuestra aplicación para que otros usuarios la prueben?

## Dockerfile

***

Es un archivo de texto que tiene todos los comandos con la que podemos contruir una imagen personalizada de Docker en base a una imagen predefinida.

## Estructura de un Dockerfile

Según la **[documentación](https://docs.docker.com/engine/reference/builder/)**, las directivas más importantes que debe tener un Dockerfile son los siguientes:

**FROM** – Aquí definimos la imagen base que se va a utilizar para contruír nuestra imagen.

**RUN** – Son los comandos que vamos a ejecutar dentro de la imagen, por ejemplo, instalar paquetes, correr scripts, etc

**CMD** – Tiene una función similar al comando RUN, con la diferencia que los comados serán ejecutados una vez el contenedor sea lanzado.

**ENTRYPOINT** – es el punto de entrada de la aplicación en la imagen cuando el contenedor es creado.

**ADD** o **COPY** – Copia archivos desde el origen al destino (dentro de contenedor).

**ENV** – Con esta directiva podemos configurar variables de entorno.

Supongamos que tenemos una página web en HTML, y queremos que esa página esté disponible en nuestro contenedor, en la misma carpeta donde tenemos el proyecto o aplicacion, generamos el Dockerfile:

```bash
touch Dockerfile
```

Y la abrimos con nuestro editor de textos de confianza.

```dockerfile
# Con el numeral podemos colocar comentarios

# Aqui definimos nuestra imagen base, recomiendo usar imagenes con alpine
# ya que es una distro muy pequeña que tiene lo basico para funcionar
FROM alpine

# Instalamos nginx
RUN apk --no-cache add nginx

# Añadimos el index.html al contenedor en la ruta donde sirve nginx 
ADD index.html /var/www/html/

# Exponemos el puerto 80 en el contenedor
EXPOSE 80

# Ejecutamos nginx
CMD [“nginx”,”-g”,”daemon off;”]
```

Para construir la imagen, ejecutamos el siguiente comando:  

```bash
docker build -t usuario/app:latest .
```

`-t`: especificamos una etiqueta o un Tag, si no lo colocamos la imagen no tendrá nombre y en caso de querer borrarla tendremos que eliminarla con el ID.  

`latest`: le indicamos a Docker que contruya la imagen y la etiquete como la mas actual. Es recomendado y una buena práctica colocarle también la versión de nuestra aplicación, podemos colocar `v1`, `v2`, etc.  

`usuario`: el nombre de nuestra cuenta en DockerHub.  

`app`: el nombre de nuestra imagen.  

El punto al final no es un error de typo (no te asustes), ese punto al final lo que le va a indicar a Docker es que queremos contruír la imagen en base al directorio actual donde nos encontramos y que haya un Dockerfile. Si queremos especificarle un Dockerfile específico (se me ocurre uno para test y otro para prod), ejecutamos:  

```bash
docker build -f Dockerfile.test -t usuario/app:tag .
docker build -f Dockerfile.prod -t usuario/app:tag .
```

## Subir Nuestra Imagen a DockerHub

Antes que nada, **[Regístrate](https://hub.docker.com/signup)** es grátis, y luego que tengas tus credenciales, ejecutamos el siguiente comando en una terminal:

```bash
docker login 
```

Nos pedirá nuestro usuario (solo el usuario, no el email) y nuestra contraseña:  

```text
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: usuario_obvio
Password: password_mas_que_obvio
WARNING! Your password will be stored unencrypted in /home/usuario/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Para envíar nuestra imagen a DockerHub ejecutamos el siguiente comando:  

```bash
docker push usuario/app:latest
```

Y subir la version específica:  

```bash
docker push usuario/app:v1
```

Si todo va bien, entramos a nuestro DockerHub con nuestro usuario, y podremos ver la imagen generada :smile:

## Multi-Stage Builds

Hay casos donde queremos utilizar una imagen base para compilar toda nuestra aplicación (Java, Go, Angular, React etc.) esta resulta ser muy pesada para colocarla en producción, ya que contiene muchas bibliotecas que podríamos omitir; y luego de eso colocar nuestra aplicación ya compilada en una imagen limpia. Para este caso particular usaremos **[Multi-Stage Builds](https://docs.docker.com/develop/develop-images/multistage-build/)**

```dockerfile
# Imagen que vamos a usar para compilar la aplicación
FROM golang:1.16 as BUILDER
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# Imagen limpia donde vamos a colocar la aplicación
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/github.com/alexellis/href-counter/app ./
CMD ["./app"]  
```

Como podemos ver, en la linea donde dice `FROM golang:1.16 as BUILDER` **BUILDER** es un alias que le colocamos al Dockerfile para que tenga en cuenta que se trata de la imagen donde vamos a compilar nuestra aplicación. La segunda imagen será la más livianita donde vamos a colocar nuestra aplicación.

Contruímos la imagen:  

```bash
docker build -t usuario/goapp:latest .
docker build -t usuario/goapp:v1 .
```

Algo super importante, es que cuando le haces cualquier cambio a tu código o necesitas instalar algún paquete a la imagen, Docker almacena las capas (que no son más que comandos ejecutados) y las próximas contrucciones serán mucho más rápidas.  

Para finalizar la subimos a DockerHub:  

```bash
docker push usuario/goapp:latest
docker push usuario/goapp:v1
```

## Probando Nuestra Imagen

Bueno, solo nos resta probar si la imagen funciona:  

```bash
docker run -d -p 80:8080 --name app app:latest
```

Exponemos el puerto 80 de contenedor al puerto 8080 del host.  

Con el siguiente comando nos deberia responder nuestra página web.  

```bash
curl localhost:8080
```

Hasta aquí el artículo, espero les haya gustado, ¡hasta la próxima!

## Referencias

***

* <https://docs.docker.com/engine/reference/builder/>
* <https://docs.docker.com/develop/develop-images/dockerfile_best-practices/>
* <https://docs.docker.com/develop/develop-images/multistage-build/>

## Artículos sobre Docker

***

* [Como Instalar Docker en Linux](https://www.itsimplenow.com/como-instalar-docker-en-linux/)
* [Como Instalar Portainer: El Mejor Gestor Gráfico de Docker en Linux](https://www.itsimplenow.com/como-instalar-portainer-el-mejor-gestor-grafico-de-docker-en-linux/)  
* [Conceptos y Comandos Básicos en Docker](https://www.itsimplenow.com/conceptos-y-comandos-basicos-en-docker/)
* Construyendo Imágenes Personalizadas en Docker con Dockerfile
* [Desplegando Aplicaciones con Docker Compose](https://www.itsimplenow.com/desplegando-aplicaciones-con-docker-compose)
* [Como Configurar un Registro Privado de Docker en Linux](https://www.itsimplenow.com/como-configurar-un-registro-privado-de-docker-en-linux)
* [SupervisorD: Gestionando Procesos en Docker](https://www.itsimplenow.com/supervisord-gestionando-procesos-en-docker/)
* [Buenas Prácticas al Escribir Dockerfiles](https://www.itsimplenow.com/buenas-practicas-al-escribir-dockerfiles/)
* [Crear Imágenes Multi-Arquitectura en Docker con buildx](https://www.itsimplenow.com/crear-imagenes-multi-arquitectura-de-docker-con-buildx/)

## Apoya este Proyecto

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

---
title: "Oracle ofrece Entrenamiento y Certificaciones Cloud Gratis"
date: 2021-09-09
lastmod: 2022-01-12
authors: [enmanuelmoreira]
description: "En el 2020 y a raíz de la pandemia del COVID-19, Oracle disponibilizó entrenamiento y certificaciones en el área cloud de manera gratuita, lo cual representó una gran oportunidad para aquellos profesionales que estuvieran buscando conocimientos sobre la nube pública y sus servicios."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpg"

categories: ["Certificaciones"]
tags: ["oracle", "oci", "cloud", "certificaciones"]

lightgallery: true
---

<!--more-->

# ACTUALIZACION 12/01/2022

Las certificaciones Oracle fueron extendidas hasta el **28/02/2022** por lo que te recomiendo encarecidamente hagas los paths a continuación para poder obtener las certificaciones correspondientes.

¡Hola a todos!

En el 2020 y a raíz de la pandemia del COVID-19, Oracle [disponibilizó](https://www.oracle.com/corporate/blog/free-certifications-oracle-oci-autonomous-033020.html) entrenamiento y certificaciones en el área cloud de manera gratuita, lo cual representó una gran oportunidad para aquellos profesionales que estuvieran buscando conocimientos sobre la nube pública y sus servicios. Un año despues, Oracle lo vuelve a hacer, y anuncia un programa de capacitación renovado lo cual incluye lo siguiente:

* Acceso a todo el catálogo de entrenamiento digital de Oracle Cloud Infrastructure el cual incluye cursos para todos los niveles y roles, con soporte en 13 idiomas.
* Laboratorios y prácticas a través del nivel Free Tier de OCI para praticar en un entorno real.
* Sesiones en vivo realizados por expertos de Oracle quienes cubrirán las mejores prácticas y proveer de retroalimentación personalizada.
* Recursos orientados a aquellos profesionales que se encuentran en búsqueda de nuevas oportunidades laborales.

{{< tweet 1435637605668769795 >}}

## Entrenamientos y Certificaciones Disponibles

***

Las Certificaciones de Oracle Cloud están divididas en 4 clases:

* **Architect:** para aquellos profesionales involucrados con la arquitectura de servicios de Oracle Cloud.
* **Operations:** para aquellos profesionales involucrados con la operación de servicios de Oracle Cloud.
* **Developer:** para aquellos profesionales desarrolladores de producto y sistemas en Oracle Cloud.
* **Specialist:** orientada a aquellos profesionales quienes desean especializarse en un área en específico, como base de datos, analíticas de la nube, gestión de productos, etc.

!["Path de Certificaciones"](/images/oracle-free-certs/oracle-cloud-certification-path.jpg)

Están disponibles las siguientes certificaciones:

### Área Cloud

* [OCI Foundations Associate 2021 - 7+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oci-foundation-associate/35644/98057)
* [OCI Architect Associate 2021 - 13+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oci-architect-associate/35644/98012)
* [OCI Cloud Operations Associate 2021 - 10+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oci-cloud-operations-associate/35644/98023)
* [OCI Architect Professional 2021 - 15+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-oci-architect-professional/35644/97984)
* [OCI Developer Associate 2021 - 8+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-oci-developer-associate/35644/99037)

### Base de Datos

* [OCI Autonomous Database Specialist 2021 - 8+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-autonomous-database-specialist/35644/97809)
* [Oracle Cloud Database Services Specialist 2021 - 8+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oracle-cloud-database-services-specialist/35644/91401)
* [Oracle Cloud Database Migration and Integration Specialist 2021 - 5+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oracle-cloud-database-migration-and-integration-specialist/35644/92250)

### Desarrollo de Aplicaciones

* [Oracle Cloud Platform Digital Assistant Specialist 2021 - 8+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-a-digital-assistant-developer/35644/91396)
* [Oracle Cloud Platform Application Development Specialist 2021 - 58+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-a-cloud-app-developer/35644/91389)
* [Oracle Cloud Platform Content Management Specialist 2021 - 12+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-a-content-management-specialist/35644/57593)
* [Oracle Cloud Platform Application Integration Specialist 2021 - 38+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-application-integration-specialist/35644/98851)
  
### Analitycs Cloud

* [Oracle Cloud Platform Content Enterprise Analitycs Specialist 2021 - 39+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-a-business-analytics-expert/35644/91371)
* [Oracle Cloud Platform Systems Management Specialist 2021 - 17+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-an-oracle-management-cloud-administrator/35644/91205)

### Seguridad

* [Oracle Cloud Platform Identity and Security Management Specialist 2021 - 20+ hrs de contenido](https://learn.oracle.com/ols/learning-path/become-a-cloud-security-administrator/35644/98653)

{{< admonition info >}}
Todo el contenido de los entrenamientos y las certificaciones estarán disponibles sin ningún costo hasta el 31 de Diciembre de 2021.
{{< /admonition >}}

## ¿Cómo tener acceso?

***

Para acceder al contenido y comenzar a aprender, debemos seguir los siguientes pasos:

1. Crea una cuenta gratuita de Oracle. Si ya tuvieras una cuenta puedes omitir esta etapa.

* Accesa al link **<https://education.oracle.com/>**
* Haz click en la esquina superior derecha en el icono de perfil (al lado de la bandera del país donde te toque), luego **Crear Cuenta**, completa los campos requeridos.
* Te van a mandar un correo electrónico de verificación, click en el enlace que te aparezca para activar tu cuenta.

2. Accede al path de aprendizaje de la [**certificación**](#área-cloud) que quieras hacer.

3. Una vez hayas completado todo el entrenamiento y las prácticas, regístrate y haz tu examen de certificación gratuito.

## ¿Se Necesita Realmente Experiencia Previa?

Respuesta corta: **NO**

**Respuesta larga:** Si eres una persona que ha trabajado en el área de Operaciones/Soporte Técnico se te va a hacer bastante facil comprender los conceptos de VPC, Instancias, LB, etc. En caso de ser tu primera experiencia en el área informática, te recomendaría al menos tener noción de Linux, Base de Datos y Redes. La práctica va a ser muy importante para comprender todo el ecosistema de servicios que ofrece cualquier nube pública (fuera de los nombres rimbombates que tienen)

Para finalizar, a pesar que Oracle Cloud sea una de las nubes menos populares en la actualidad, vale la pena sacar las certificaciones, ya que si te estás adentrando en este mundo es una oportunidad de oro para especializarte.

Espero les haya gustado ¡Hasta la próxima!

## Apoya este Proyecto!!!

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

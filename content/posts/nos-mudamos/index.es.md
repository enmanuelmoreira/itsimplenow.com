---
title: "Nos Mudamos!"
date: 2022-12-01
lastmod: 2022-11-01
authors: [enmanuelmoreira]
description: "Nos mudamos a blog.enmanuelmoreira.com"
draft: false

resources:
- name: "featured-image"
  src: "featured-image.png"

categories: ["OffTopic"]
tags: []

lightgallery: true
---

<!--more-->

## Actualización al 01/01/2022

Despues de dos años con el blog, he decidido mudarlo a <https://blog.enmanuelmoreira.com>

Si estabas siguiendo el contenido de este blog, es solo acceder al nuevo link.

Mis links por si quieres disfrutar de mas contenido <https://enmanuelmoreira.com>

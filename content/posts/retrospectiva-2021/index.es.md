---
title: "Retrospectiva del año 2021"
date: 2021-12-31
lastmod: 2021-12-31
authors: [enmanuelmoreira]
description: ""
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpg"

categories: []
tags: []

lightgallery: true
---

Retrospectiva de mi año 2021:  

Desarrollo de mis habilidades profesionales.  

Tomé cursos de:  
:cloud: Google Cloud.  
:cloud: GitLab Associate.  
:cloud: Kubernetes.  
:cloud: Jenkins.  

:persevere: Cambié de empleo. Conseguí después de 2 largos años de esfuerzo comenzar como DevOps en una consultora.

Es por esto que decidí, al menos este año, no presentar los exámenes Azure Administrator AZ-104 ni AWS Solutions Architect Associate, ya que en la empresa donde trabajo manejan las cosas con GCP, GitLab y enfocar mi esfuerzo hacia esas herramientas.  

Mis expectativas para el año 2022:  
Pasar los exámenes:  
:mortar_board: Google Associate Cloud Engineer.
:mortar_board: Certified Kubernetes Administrator.  
:mortar_board: AWS Solutions Architect Associate. (En veremos)  

:white_check_mark: Ganar más experiencia y confianza en cuanto a la cultura DevOps.  
:white_check_mark: Mejorar habilidades de comunicación efectiva.  
:writing_hand: Seguir escribiendo artículos para el blog.  
:video_camera: Hacer contenido para Youtube. (Este año si que si)  
:white_check_mark: Aprender un CD (Flux o ArgoCD).
:white_check_mark: Aprender un Api Gateway (Kong, Konga).

Reforzar conocimientos de:  
:white_check_mark: CI/CD.  
:white_check_mark: Jenkins.  

Todo esfuerzo tiene su recompensa, y tu, ¿estás listo?. :muscle:  

¡Hasta la próxima!

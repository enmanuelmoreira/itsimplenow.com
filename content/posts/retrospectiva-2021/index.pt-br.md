---
title: "Retrospectiva do ano 2021"
date: 2021-12-31
lastmod: 2021-12-31
authors: [enmanuelmoreira]
description: ""
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpg"

categories: []
tags: []

lightgallery: true
---

Retrospectiva do meu ano 2021:  

Desenvolvimiento das minhas habilidades professionáis.  

Tomei aulas virtuáis de:  
:cloud: Google Cloud.  
:cloud: GitLab Associate.  
:cloud: Kubernetes.  
:cloud: Jenkins.  

:persevere: Mudei de emprego. Conseguí após 2 anos de esforço começar como numa consultora.

Por isto decidi, pelo menos este ano, nao apresentar os testes Azure Administrator AZ-104 nem AWS Solutions Architect Associate, já que na empresa onde trabalho gestionam mais com a GCP, GitLab e focar o meu esforço nessas ferramentas.  

As minhas expectativas para este novo ano 2022:  
Aprovar os testes:  
:mortar_board: Google Associate Cloud Engineer.
:mortar_board: Certified Kubernetes Administrator.  
:mortar_board: AWS Solutions Architect Associate. (En curso)  

:white_check_mark: Ganhar mais experiência e confiança quanto à cultura DevOps.  
:white_check_mark: Melhorar habilidades de comunicação efectiva.  
:writing_hand: Continuar a escrever artígos no blog.  
:video_camera: Fazer conteúdo no Youtube (este ano sim ou sim)
:white_check_mark: Aprender um CD (Flux o ArgoCD).
:white_check_mark: Aprender um Api Gateway (Kong, Konga).  

Reforçar os meus conhecimentos sob:  
:white_check_mark: CI/CD.  
:white_check_mark: Jenkins.

Tudo esforço será recompensado, e tu, estás pronto?. :muscle:  

Até a próxima!

---
title: "Como Crear y Configurar un Cluster de Base de Datos de MariaDB con Galera en CentOS 8 / RHEL 8 / AlmaLinux 8 / Rocky Linux 8"
date: 2021-07-15
lastmod: 2021-07-15
authors: [enmanuelmoreira]
description: "El balanceo de carga y clustering en un ambiente de producción, son muy importantes para alcanzar la alta disponibilidad (High Availability o HA). MariaDB Galera Cluster provee una solución de cluster multi-master lo que permite que cada modificación de datos sea replicada a todos los otros nodos (a diferencia de MySQL Cluster donde hay un nodo Administrador y nodos de Datos) Galera tambien soporta entornos WAN y CLoud para distribuir los datos a traves de regiones."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.png"

categories: ["Linux", "Base de Datos", "Tutoriales"]
tags: ["redhat","centos","mariadb","base de datos","cluster", "galera", "cluster"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente enlace: <https://bit.ly/digitalocean-itsm>

***

El balanceo de carga y clustering en un ambiente de producción, son muy importantes para alcanzar la alta disponibilidad (High Availability o HA). MariaDB Galera Cluster provee una solución de clúster multi-master lo que permite que cada modificación de datos sea replicada a todos los otros nodos (a diferencia de MySQL Clusters donde hay un nodo Administrador y nodos de Datos) Galera también soporta entornos WAN y Cloud para distribuir los datos a través de regiones.  

En esta guía vamos a instalar un clúster de MariaDB Galera en CentOS 8 / RHEL 8 / AlmaLinux 8 / Rocky Linux 8.

## Requisitos  

***

### Hardware

Vamos a necesitar 3 nodos con las siguientes características (para efectos del laboratorio, virtuales):

- 1 vCPU
- 1024 MB de memoria RAM
- 5 GB de espacio en disco

| Hostname              |       IP    |
|-----------------------|-------------|
| galera-nodo1.home.lab | 192.168.1.10|
| galera-nodo2.home.lab | 192.168.1.11|
| galera-nodo3.home.lab | 192.168.1.12|

### Configurando archivo /etc/hosts en cada Nodo

Para que nos sea mucho más fácil poder resolver cada nodo con su Dirección IP, vamos a editar el archivo /etc/hosts de cada nodo (con su editor de textos de confianza, yo usaré vim) y colocar al final del archivo lo siguiente:

```bash
vim /etc/hosts
```

```bash
192.168.1.10 galera-nodo1.home.lab galera-nodo1
192.168.1.11 galera-nodo2.home.lab galera-nodo2
192.168.1.12 galera-nodo3.home.lab galera-nodo3
```

### Instalando MariaDB en cada Nodo

Debemos instalar MariaDB en cada Nodo, ya hice una guía sobre esto hace algún tiempo: **[Como Instalar MariaDB en CentOS 8 / RHEL 8](https://www.itsimplenow.com/como-instalar-mariadb-en-centos-8-rhel-8/)**

## Abriendo los Puertos en el Firewall

***

Vamos a abrir los puertos que son necesarios para que el cluster funcione:

- El puerto 3306 para las conexiones desde el cliente de MariaDB y el State Snapshot Transfer que usa el comando mysqldump.  
- El puerto 4567 en TCP y UDP para permitir la replicación de datos de Galera entre los nodos.  
- El puerto 4568 para los Incremental State Transfers, or IST, proceso mediante el cual un estado faltante es recibido por los otros nodos del cluster.  
- El puerto 4444 para todos los otros estados del State Snapshot Transfers, or SST, mecanismo mediante el cual un nodo que se une al cluster recibe el estado y los datos de otro nodo.  

Ejecutamos la serie de comandos a continuación en cada Nodo:  

```bash
sudo firewall-cmd --permanent --zone=public --add-port=3306/tcp
sudo firewall-cmd --permanent --zone=public --add-port=4567/tcp
sudo firewall-cmd --permanent --zone=public --add-port=4568/tcp
sudo firewall-cmd --permanent --zone=public --add-port=4444/tcp
sudo firewall-cmd --permanent --zone=public --add-port=4567/udp
```

Como vemos de los comandos anteriores, estamos usando la zona public en el firewall, ahora debemos añadir cada IP con su respectiva notación CIDR del nodo a la zona public (ejecutar también en cada Nodo):  

```bash
sudo firewall-cmd --permanent --zone=public --add-source=192.168.1.10/24 #galera-nodo1
sudo firewall-cmd --permanent --zone=public --add-source=192.168.1.11/24 #galera-nodo2
sudo firewall-cmd --permanent --zone=public --add-source=192.168.1.12/24 #galera-nodo3
```

Recargamos el Firewall:  

```bash
sudo firewall-cmd --reload
```

## Configurando Galera en los Nodos

***

{{< admonition tip >}}
Todos los pasos a continuación, los vamos a ejecutar en TODOS los Nodos.
{{< /admonition >}}

Primero que nada vamos a detener el servicio de MariaDB:  

```bash
sudo systemctl stop mariadb
```

Ahora, vamos a crear el archivo de configuración del clúster en la ruta /etc/my.cnf.d/  

```bash
sudo vim /etc/my.cnf.d/galera.cnf
```

Y pegamos lo siguiente:  

```bash
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="galera_cluster"
wsrep_cluster_address="gcomm://node1-ip-address,node2-ip-address,node3-ip-address"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="node1-ip-address"
wsrep_node_name="node1"
```

Prestemos atención a los valores:  

- La primera sección modifica los parámetros de MariaDB para que permita al clúster su correcto funcionamiento. Podemos observar como el motor por defecto es InnoDB en vez de MyISAM o cualquier otro motor no transaccional. Además de esto, el valor del parámetro  bind-address en 0.0.0.0 permitirá la conexión desde cualquier dirección IP.  

- La sección “Galera Provider Configuration” configura los componentes de MariaDB que proveen una API replicación WriteSet. Es decir, en nuestro caso, Galera es un proveedor wsrep (WriteSet Replication)  

- La sección “Galera Cluster Configuration” define el clúster, identificando los miembros del mismo por sus direcciones IP o por sus nombres DNS y crea un nombre para el cluster el cual debe ser igual para todos los nodos (parametro **wsrep_cluster_name**) El valor del parámetro **wsrep_cluster_address** debe tener las direcciones IP de los tres nodos que estamos configurando.  

- La sección “Galera Synchronization Configuration” define como el cluster se comunicará y sincronizará los datos entre nodos. Esto es usado solamente para la transferencia de estado el cual sucede cuando el nodo pasa a estado Online. Para la configuración inicial, se usa  rsync porque es está comunmente disponible y es lo que se necesita de momento.  

- Y para finalizar, la sección “Galera Node Configuration” muestra claramente la dirección IP y el nombre del Nodo, lo que ayuda a diagnosticar problemas en los logs. El parámetro  **wsrep_node_address** debe coincidir con las direcciones IP de los Nodos, sin embargo, se puede escoger cualquier nombre para ayudar a identificar cualquier problema en los logs del Nodo.  

Dicho esto, modifiquemos los valores por cada nodo:  

**wsrep_cluster_name=**"Nombre del cluster(personalizado)"  
**wsrep_cluster_address=**"gcomm://Direcciones IP de todos los nodos separados por comas"  

**wsrep_node_address=**"Direccion IP de Cada Nodo"  
**wsrep_node_name=**"Nombre del Nodo"  

- **Nodo galera-nodo1**

```bash
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="galera_cluster"
wsrep_cluster_address="gcomm://192.168.1.10,192.168.1.11,192.168.1.12"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="192.168.1.10"
wsrep_node_name="galera-nodo1"
```

- Nodo **galera-nodo2**
  
```bash
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="galera_cluster"
wsrep_cluster_address="gcomm://192.168.1.10,192.168.1.11,192.168.1.12"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="192.168.1.11"
wsrep_node_name="galera-nodo2"
```

- Nodo **galera-nodo3**
  
```bash
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_cluster_name="galera_cluster"
wsrep_cluster_address="gcomm://192.168.1.10,192.168.1.11,192.168.1.12"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="192.168.1.12"
wsrep_node_name="galera-nodo3"
```

## Inicializando el Clúster

***

Hasta aquí, ya debemos tener configurados los nodos de manera satisfactoria.  

Con el servicio MariaDB detenido en todos los nodos, inicializamos el Clúster Galera con el siguiente comando:  

```bash
sudo galera_new_cluster
```

Este comando normalmente no va a mostrar ninguna salida por el stdout si se hubiere ejecutado con éxito, dando por sentado que ya el nodo está unido al clúster y además el servicio MariaDB va a ser iniciado por Galera de manera automáticas.  La manera que tenemos de saberlo, es con el siguiente comando:  

```bash
mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_cluster_size'"
```

El cual debería mostrarnos una salida parecida a esta:  

```bash
+--------------------+-------+
| Variable_name      | Value |
+--------------------+-------+
| wsrep_cluster_size | 1     |
+--------------------+-------+
```

Uno a uno vamos uniendo los demás nodos al cluster y consultando para ver si están uniéndose de manera correcta.  

## Probando la Replicación

Por último, vamos a probar si realmente cualquier cambio que hagamos en cualquier nodo, los cambios serán replicados a los otros, vamos a entrar con el cliente mysql en el nodo **galera-nodo1**:  

```bash
mysql -u root -p
```

Una vez estemos conectados, creemos una base de datos de ejemplo:  

```bash
MariaDB [(none)]> CREATE DATABASE db1;
```

Salimos de MariaDB y vamos a conectarnos al nodo **galera-nodo2**:  

```bash
mysql -u root -p
```

Y consultamos todas las bases de datos:  

```bash
MariaDB [(none)]> SHOW DATABASES;
```

Podemos observar que la base de datos db1 ha sido replicada al nodo2:  

```bash
+--------------------+
| Database           |
+--------------------+
| db1                | 
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
5 rows in set (0.001 sec)
```

Y verificamos en el nodo **galera-nodo3**:  

```bash
MariaDB [(none)]> SHOW DATABASES;
```

```bash
+--------------------+
| Database           |
+--------------------+
| db1                | 
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
5 rows in set (0.001 sec)
```

Espero les haya gustado este tutorial, ¡hasta la próxima!

## Apoya este Proyecto!!!

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>

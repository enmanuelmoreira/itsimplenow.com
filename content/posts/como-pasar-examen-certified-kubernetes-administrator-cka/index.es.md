---
title: "Como pasar el examen Certified Kubernetes Administrator CKA"
date: 2022-03-15
lastmod: 2022-03-15
authors: [enmanuelmoreira]
description: "Hoy les contaré mi experiencia para estudiar y pasar el examen Certified Kubernetes Administrator, comúnmente conocido como CKA."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpeg"

categories: ["Certificaciones"]
tags: ["kubernetes", "cka", "cloud", "certificaciones", "automatizacion", "infraestructura"]

lightgallery: true
---

<!--more-->

¡Hola a todos!

Hoy les contaré mi experiencia para estudiar y pasar el examen Certified Kubernetes Administrator (CKA).  

Es un examen bastante exigente. No es del tipo de examen en la que tienes que escoger entre múltiples opciones, es uno práctico en el cual debes realizar una serie de tareas y hacer troubleshooting.  

Los **[tópicos](https://docs.linuxfoundation.org/tc-docs/certification/lf-candidate-handbook)** que debemos estudiar son los siguientes:  

:book: **Storage** 10%  

* Understand storage classes, persistent volumes
* Understand volume mode, access modes and reclaim policies for volumes
* Understand persistent volume claims primitive
* Know how to configure applications with persistent storage

:book: **Troubleshooting** 30%  

* Evaluate cluster and node logging
* Understand how to monitor applications
* Manage container stdout & stderr logs
* Troubleshoot application failure
* Troubleshoot cluster component failure
* Troubleshoot networking

:book: **Workloads & Scheduling** 15%  

* Understand deployments and how to perform rolling update and rollbacks
* Use ConfigMaps and Secrets to configure applications
* Know how to scale applications
* Understand the primitives used to create robust, self-healing, application deployments
* Understand how resource limits can affect Pod scheduling
* Awareness of manifest management and common templating tools

:book: **Cluster Architecture, Installation & Configuration** 25%  

* Manage role based access control (RBAC)
* Use Kubeadm to install a basic cluster
* Manage a highly-available Kubernetes cluster
* Provision underlying infrastructure to deploy a Kubernetes cluster
* Perform a version upgrade on a Kubernetes cluster using Kubeadm
* Implement etcd backup and restore

:book: **Services & Networking** 20%  

* Understand host networking configuration on the cluster nodes
* Understand connectivity between Pods
* Understand ClusterIP, NodePort, LoadBalancer service types and endpoints
* Know how to use Ingress controllers and Ingress resources
* Know how to configure and use CoreDNS
* Choose an appropriate container network interface plugin

## Materiales de Estudio

***

Además de la **[documentación oficial](https://kubernetes.io/es/docs/home/)**, utilicé los siguientes recursos:  

* Curso de Udemy **[Certified Kubernetes Administrator](https://www.udemy.com/course/certified-kubernetes-administrator)** de [Zeal Vora](https://www.udemy.com/user/cybercorp/) cuesta aproximadamente USD 30.00.  

* El Curso **[KUBERNETES 2021 - De NOVATO a PRO! (CURSO COMPLETO)](https://www.youtube.com/watch?v=DCoBcpOA7W4)** de Pelado Nerd en su **[canal de Youtube](https://www.youtube.com/channel/UCrBzBOMcUVV8ryyAU_c6P5g)**, el cual recomiendo ampliamente suscribirse)  

* El Curso **[Kubernetes Tutorial for Beginners (FULL COURSE in 4 Hours)](https://www.youtube.com/watch?v=X48VuDVv0do)** de Nana Janaisha en su **[canal de Youtube](https://www.youtube.com/channel/UCdngmbVKX1Tgre699-XLlUA)**, también recomiendo suscribirse)  

* **[Kubernetes Cheat Sheet](https://github.com/dennyzhang/cheatsheet-kubernetes-A4/blob/master/cheatsheet-kubernetes-A4.pdf)**: Una lista con comandos útiles.  

* **[Laboratorios de Práctica](https://github.com/devopshubproject/cka-lab)**

* **[50 Preguntas para los Exámenes CKA y CKAD](https://dev.to/subodev/50-questions-for-ckad-and-cka-exam-3bjm)**

* **[Lista de Favoritos de la Documentación de Kubernetes](/files/cka/cka-bookmarks.html)**

## El Examen

***

* El registro se hace con la empresa PSI a través de Linux Foundation con tu cuenta de correo, haciendo click [aquí](https://training.linuxfoundation.org/full-catalog/?_sft_product_type=certification#)  
* Costo actual: $375 (Te da derecho a 2 intentos, así que aprovecha sabiamente :smile:)  
* Idioma: Inglés  
* Formato: Examen Práctico  
* Modalidad: Proctored  
* Duración: 120 minutos  
* Número de Preguntas: de 15 a 19  
* Nota mínima para aprobar: 66%  
* Puedes presentarlo desde la comodidad de tu casa.  
* Una vez que compras el examen tienes 12 meses para presentarlo.  
* En caso de aprobar, la certificación es válida por 3 años.  

Adicionalmente, puedes descargar tu Placa en **[Credly](https://www.credly.com/)** y compartir tu felicidad por Linkedin :)  

## Consejos al momento de presentar la prueba

***

* Debes tener una PC o Laptop que tenga cámara web y acceso al micrófono.  
* Una buena conexión a Internet.  
* Trata de iniciar sesión al menos 15 minutos antes de la prueba, para que puedas seguir los pasos que te indica el software de PSI.  
* Estar en un lugar tranquilo y bien iluminado, depende del proctor que te toque puede ser bastante laxo o bastante exigente.  
* Trata de hacer el examen por la mañana, ya que son 2 horas en las que tienes que aprovechar el tiempo lo más posible, te van a permitir tener una botella de agua en el escritorio, y si necesitas por alguna razón ir al baño o hacer un brake, te lo van a descontar del tiempo que te quede.  
* Tener tu DNI a la mano o Pasaporte **VIGENTE**  
* Navegadores soportados: Mozilla Firefox, Google Chrome.  

En cuanto al examen como tal:  

* Te van a permitir consultar solo la documentación oficial de Kubernetes. Si abres alguna otra página probablemente te bloqueen la prueba.  

* Trata de usar comandos imperativos para ahorrar tiempo, por ejemplo, para crear un pod: `kubectl run nginx --image=nginx`; si necesitas crear un yaml, colócale el argumento `--dry-run=client`.  

* Marca aquellas preguntas en la que sabes que te vas a demorar en responder, o que necesitas consultar la documentación.  

Si bien la información oficial de la certificación indica que no es necesario ningún prerequisitos, debes tener en cuenta que como mínimo debes saber Linux y Docker.  

Con todo lo anterior, dedicándole dos horas al día durante 1 mes y medio, el pasado día 27/02/2022 pasé la prueba y obtuve la certificación!!!

![Certificado](/images/cka/cka.jpg "Mi preciada Certificación")

Espero les haya gustado ¡Hasta la próxima!

## Apoya este Proyecto!!!

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>
